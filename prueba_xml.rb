


require 'rubygems'
require 'bundler'
require 'active_support/core_ext'
require 'pp'

def datosParticularesx(codRamo,c)
    def robos(d)#SeguroIntegral
        if d['Robo'].nil?
            return nil
        end
        r = d['Robo']
        puts 'robo'
        puts r['Zona']
        rob=Robo.create( Zona: r['Zona'], Actividad: r['Actividad'], ValorRiesgo: r['ValorRiesgo'], ModalidadSeguro: r['ModalidadSeguro'], TipoRiesgo: r['TipoRiesgo'], Moneda: r['Moneda'])
        return rob
    end
    def incendios(d)#seguroIntegral
        if d['Incendio'].nil?
            return nil
        end
        i = d['Incendio']
        puts 'incencio'
        puts i['Actividad']
        inc = Incendio.create(PrimaDeposito: i['PrimaDeposito'], Actividad: i['Actividad'], CantidadPisos: i['CantidadPisos'], TipoRiesgo: i['TipoRiesgo'], TipoCerramiento: i['TipoCerramiento'], PeriodoIndemnizacion: i['PeriodoIndemnizacion'], TipoTecho: i['TipoTecho'], PrimaAnticipo: i['PrimaAnticipo'])
        return inc
    end
    def items(d, tipo_d, id_d)
        if d['DatosPart.'].nil? or d['DatosPart.']['Item'].nil? #MODIFIQUE ESTOOOO
            return nil
        end
        if  d['DatosPart.']['Item'].class != Array then
            puts 'items hash'
            it = d['DatosPart.']['Item']
            puts it['TipoBien']
            Item.create(CodigoGrupo: it['CodigoGrupo'], TipoBien: it['TipoBien'], DescripcionBien: it['DescripcionBien'], Capital: it['Capital'], Franquicia: it['Franquicia'], datos_particulares_type: tipo_d, datos_particulares_id: id_d)
            puts 'ITEMS CREADO'
       
        else     
            puts 'items array'     
            d['DatosPart.']['Item'].each do |it|
                Item.create(CodigoGrupo: it['CodigoGrupo'], TipoBien: it['TipoBien'], DescripcionBien: it['DescripcionBien'], Capital: it['Capital'], Franquicia: it['Franquicia'], datos_particulares_type: tipo_d, datos_particulares_id: id_d)
                puts 'ITEMS CREADO'
            end
        end
    end
    case codRamo
    when '2100'
        puts '2100'
        if c.has_key?('DatosParticularesSeguroIntegral')then
            #puts c['DatosParticularesSeguroIntegral'].keys
            d = c['DatosParticularesSeguroIntegral']
            
            inc=incendios(d)
            rob=robos(d)
            puts 'ACAAAA '
            puts rob['id']
            puts rob['Zona']
            puts inc['id']
            puts inc['Actividad']
            dat=DatosParticularesSeguroIntegral.create(TipoSeguro: d['TipoSeguro'], Actividad: d['Actividad'], ZonaRiesgo: d['ZonaRiesgo'], ObjDiversosBienesAsegurados: d['ObjDiversosBienesAsegurados'], robo_id: rob.id, incendio_id: inc.id)
            puts "id de datos particulares si #{dat.id}"
            tipo_dat="DatosParticularesSeguroIntegral"
            items(d,tipo_dat, dat.id)
        end
    when '300'
        puts '300'
        if c.has_key?('DatosParticularesIncendio') then
            #puts c['DatosParticularesIncendio'].keys
            d = c['DatosParticularesIncendio']
            dat = DatosParticularesIncendio.create( Actividad: d['Actividad'], Detalle: d['Detalle'], TipoConstruccion: d['TipoConstruccion'], CantidadPisos: d['CantidadPisos'], TipoCerramiento: d['TipoCerramiento'], ImprimirArticulo: d['ImprimirArticulo'])
            tipo_dat="DatosParticularesIncendio"
            items(d,tipo_dat, dat.id)
        end
    when '600'
        puts '600'
        if c.has_key?('DatosParticularesAccidentesPersonales') then
            #puts c['DatosParticularesAccidentesPersonales'].keys
            d = c['DatosParticularesAccidentesPersonales']
            dat = DatosParticularesAccidentesPersonale.create( NroActividad: d['NroActividad'], ActividadAsegurado: d['ActividadAsegurado'], CodClasificacion: d['CodClasificacion'], Clasificacion: d['Clasificacion'])
            tipo_dat="DatosParticularesAccidentesPersonale"
            items(d,tipo_dat, dat.id)
        end
    
    when '1730'
        puts '1730'
        if c.has_key?('DatosParticularesVidaColectivo') then
            #puts c['DatosParticularesVidaColectivo'].keys
            d = c['DatosParticularesVidaColectivo']
            dat = DatosParticularesVidaColectivo.create( EdadReal: d['EdadReal'], EdadActuarial: d['EdadActuarial'], CapitalAsegurado: d['CapitalAsegurado'], Prima: d['Prima'], DuracionSeguro: d['DuracionSeguro'], DuracionPagos: d['DuracionPagos'], EdadMax: d['EdadMax'], FechaIngTrabajo: d['FechaIngTrabajo'], Sueldo: d['Sueldo'], MediaJornada: d['MediaJornada']) 
            tipo_dat="DatosParticularesVidaColectivo"
            items(d,tipo_dat, dat.id)
        end
    when '1800'
        puts '1800'
        if c.has_key?('DatosParticularesCombinadoFamiliar') then
            #puts c['DatosParticularesCombinadoFamiliar'].keys
            d = c['DatosParticularesCombinadoFamiliar']
            dat = DatosParticularesCombinadoFamiliar.create(TipoSeguro:d['TipoSeguro'], Actividad: d['Actividad'], ZonaRiesgo: d['ZonaRiesgo'], ObjDiversosBienesAsegurados: d['ObjDiversosBienesAsegurados'])
            tipo_dat="DatosParticularesCombinadoFamiliar"
            items(d,tipo_dat, dat.id)
        end
    else
        return nil, nil
    end
    return dat, tipo_dat
    
end

def clientes(c,cert_id)
    def telefonos(cli,client)
        if cli['Telefonos'].nil?
            return nil
        end
        if  cli['Telefonos']['Telefono'].class != Array then
          puts 'tel hash'
          t = cli['Telefonos']['Telefono']
          puts t['Numero']
          Telefono.create( numero: t["numero"], client_id: client.id)
        else     
            puts 'tel array'     
            cli['Telefonos']['Telefono'].each do |t|
                puts t['Numero']
                Telefono.create( numero: t["numero"], client_id: client.id)
            end
        end
    end
    if  c['Clientes']['Cliente'].class != Array then
          puts 'cli hash'
          cli = c['Clientes']['Cliente']
          puts cli['TipoPersona']
          client = Client.create( codRol: cli["codRol"], DescRol: cli["DescRol"], TipoPersona: cli["TipoPersona"], ApellidoNombre: cli["ApellidoNombre"], Direccion: cli["Direccion"], codCliente: cli["codCliente"], NroDocumento: cli["NroDocumento"], codTipoDocumento: cli["codTipoDocumento"], DescTipoDocumento: cli["DescTipoDocumento"], codCondicionIva: cli["codCondicionIva"], DescCondicionIva: cli["DescCondicionIva"], CategMonoributo: cli["CategMonoributo"], DescCategMonotributo: cli["DescCategMonotributo"], CodPostal: cli["CodPostal"], codLocalidad: cli["codLocalidad"], Localidad: cli["Localidad"], codProvincia: cli["codProvincia"], Provincia: cli["Provincia"], email: cli["email"], codSexo: cli["codSexo"], DescSexo: cli["DescSexo"], FechaNacimiento: cli["FechaNacimiento"], codEstadoCivil: cli["codEstadoCivil"], DescEstadoCivil: cli["DescEstadoCivil"], certificado_id: cert_id)
          telefonos(cli,client)
    else     
       puts 'cli array'     
       c['Clientes']['Cliente'].each do |cli|
          puts cli['TipoPersona']
          client = Client.create( codRol: cli["codRol"], DescRol: cli["DescRol"], TipoPersona: cli["TipoPersona"], ApellidoNombre: cli["ApellidoNombre"], Direccion: cli["Direccion"], codCliente: cli["codCliente"], NroDocumento: cli["NroDocumento"], codTipoDocumento: cli["codTipoDocumento"], DescTipoDocumento: cli["DescTipoDocumento"], codCondicionIva: cli["codCondicionIva"], DescCondicionIva: cli["DescCondicionIva"], CategMonoributo: cli["CategMonoributo"], DescCategMonotributo: cli["DescCategMonotributo"], CodPostal: cli["CodPostal"], codLocalidad: cli["codLocalidad"], Localidad: cli["Localidad"], codProvincia: cli["codProvincia"], Provincia: cli["Provincia"], email: cli["email"], codSexo: cli["codSexo"], DescSexo: cli["DescSexo"], FechaNacimiento: cli["FechaNacimiento"], codEstadoCivil: cli["codEstadoCivil"], DescEstadoCivil: cli["DescEstadoCivil"], certificado_id: cert_id)
          telefonos(cli,client)
       end
    end
end
def coverages(c,cert_id)
    if c['Coberturas'].nil?
        return nil
    end
    if  c['Coberturas']['Cobertura'].class != Array then
        puts 'cover hash'
        if c['Coberturas']['Cobertura'] then 
            cov = c['Coberturas']['Cobertura']
            puts cov['CodCobertura']
            Coverage.create(FechaInicioVigenciaModulo:cov["FechaInicioVigenciaModulo"], CodModulo: cov["CodModulo"], DescModulo: cov["DescModulo"], FechaInicioVigenciaCobertura: cov["FechaInicioVigenciaCobertura"], CodCobertura: cov["CodCobertura"], DescCobertura: cov["DescCobertura"], SumaAsegurada: cov["SumaAsegurada"], certificado_id: cert_id)
        end
    else     
        puts 'cover array'
        if c['Coberturas']['Cobertura'] then      
            c['Coberturas']['Cobertura'].each do |cov|
                puts cov['CodCobertura']
                Coverage.create(FechaInicioVigenciaModulo:cov["FechaInicioVigenciaModulo"], CodModulo: cov["CodModulo"], DescModulo: cov["DescModulo"], FechaInicioVigenciaCobertura: cov["FechaInicioVigenciaCobertura"], CodCobertura: cov["CodCobertura"], DescCobertura: cov["DescCobertura"], SumaAsegurada: cov["SumaAsegurada"], certificado_id: cert_id)
            end
        end
    end
end

def ubicacionesRiesgo(c,cert_id)
    if c['UbicacionesRiesgo'].nil?
        return nil
    end
    if  c['UbicacionesRiesgo']['UbicacionRiesgo'].class != Array then
        puts 'ubic hash'
        if c['UbicacionesRiesgo']['UbicacionRiesgo'] then 
            u = c['UbicacionesRiesgo']['UbicacionRiesgo']
            puts u['CodPostal']
            UbicacionRiesgo.create( Domicilio: u["Domicilio"], codLocalidad: u["codLocalidad"], Localidad:u["Localidad"] , CodPostal:u["CodPostal"] , codProvincia:u["codProvincia"] , Provincia:u["Provincia"] , certificado_id:cert_id )
        end
    else     
        puts 'ubic array'
        if c['UbicacionesRiesgo']['UbicacionRiesgo']  then      
            c['UbicacionesRiesgo']['UbicacionRiesgo'].each do |u|
                puts u['CodPostal']
                UbicacionRiesgo.create( Domicilio: u["Domicilio"], codLocalidad: u["codLocalidad"], Localidad:u["Localidad"] , CodPostal:u["CodPostal"] , codProvincia:u["codProvincia"] , Provincia:u["Provincia"] , certificado_id:cert_id )
            end
        end
    end
end

def recibos(c,cert_id)
    def movRecibos(r,rec_id)
        if r['MovimientosRecibos'].nil?
            return nil
        end
        if  r['MovimientosRecibos']['MovRecibo'].class != Array then
            puts 'movR hash'
            m = r['MovimientosRecibos']['MovRecibo']
            puts m['codTipoMovimiento']
            MovRecibo.create( idMovimiento:m["idMovimiento"] , codTipoMovimiento:m["codTipoMovimiento"] , DescTipoMovimiento:m["DescTipoMovimiento"] , FechaContabilizacion:m["FechaContabilizacion"] , FechaEfecto:m["FechaEfecto"] , Importe:m["Importe"] , recibo_id: rec_id)
        else     
            puts 'MOvR array'     
            r['MovimientosRecibos']['MovRecibo'].each do |m|
                puts m['codTipoMovimiento']
                MovRecibo.create( idMovimiento:m["idMovimiento"] , codTipoMovimiento:m["codTipoMovimiento"] , DescTipoMovimiento:m["DescTipoMovimiento"] , FechaContabilizacion:m["FechaContabilizacion"] , FechaEfecto:m["FechaEfecto"] , Importe:m["Importe"] , recibo_id: rec_id)
            end
        end
    end
    def comisiones(r,rec_id)
        if r['Comisiones'].nil?
            return nil
        end
        if  r['Comisiones']['ComisionGenerada'].class != Array then
            puts 'COmision hash'
            co = r['Comisiones']['ComisionGenerada']
            puts "intermed #{co['Intermed']}"
            ComisionGenerada.create( Intermed:co["Intermed"] , Monto:co["Monto"] , Porcentaje:co["Porcentaje"] , recibo_id: rec_id)
        else     
            puts 'comision array'     
            r['Comisiones']['ComisionGenerada'].each do |co|
                puts "intermed #{co['Intermed']}"
                ComisionGenerada.create( Intermed:co["Intermed"] , Monto:co["Monto"] , Porcentaje:co["Porcentaje"] , recibo_id: rec_id)
            end
        end
    end
    if c['Recibos'].nil?
        return nil
    end

    if  c['Recibos']['Recibo'].class != Array then
          puts 'reci hash'
          r = c['Recibos']['Recibo']
          puts r['NroRecibo']
          rec = Recibo.create( NroRecibo:r["NroRecibo"] , NroReciboBase:r["NroReciboBase"] , VigenciaDesde:r["VigenciaDesde"] , VigenciaHasta:r["VigenciaHasta"] , FechaVencimiento:r["FechaVencimiento"] , NroTransaccion:r["NroTransaccion"] , FactorCambio:r["FactorCambio"] , ImportePremio:r["ImportePremio"] , ImporteSaldo:r["ImporteSaldo"] , ImportePrima:r["ImportePrima"] , Periodo:r["Periodo"] , ImporteIVAGeneral:r["ImporteIVAGeneral"] , ImporteTasasImpuestos:r["ImporteTasasImpuestos"] , ImpuestoSellos:r["ImpuestoSellos"] , CapitalSocial:r["CapitalSocial"] , certificado_id:cert_id) 
          movRecibos(r,rec.id)
          comisiones(r,rec.id)
    else     
       puts 'reci array'     
       c['Recibos']['Recibo'].each do |r|
          puts r['NroRecibo']
          rec = Recibo.create( NroRecibo:r["NroRecibo"] , NroReciboBase:r["NroReciboBase"] , VigenciaDesde:r["VigenciaDesde"] , VigenciaHasta:r["VigenciaHasta"] , FechaVencimiento:r["FechaVencimiento"] , NroTransaccion:r["NroTransaccion"] , FactorCambio:r["FactorCambio"] , ImportePremio:r["ImportePremio"] , ImporteSaldo:r["ImporteSaldo"] , ImportePrima:r["ImportePrima"] , Periodo:r["Periodo"] , ImporteIVAGeneral:r["ImporteIVAGeneral"] , ImporteTasasImpuestos:r["ImporteTasasImpuestos"] , ImpuestoSellos:r["ImpuestoSellos"] , CapitalSocial:r["CapitalSocial"] , certificado_id:cert_id) 
          movRecibos(r,rec.id)
          comisiones(r,rec.id)
       end
    end
end
def forma_pagos(c)
    if c['FormaPago'].nil?
        return nil
    end
    f = c['FormaPago']
    puts 'forma pago'
    puts f['TipoPago']
    forma = FormaPago.create(FechaInicioVigencia: f['FechaInicioVigencia'], TipoPago: f['TipoPago'], DescTipoPago: f['DescTipoPago'], NroCBU: f['NroCBU'], CodBanco: f['CodBanco'])
    return forma
end
def movimientos(c,cert_id)
    if c['Movimientos'].nil?
        return nil
    end
    if  c['Movimientos']['Movimiento'].class != Array then
        puts 'movimientos hash'
        if c['Movimientos']['Movimiento'] then 
            m = c['Movimientos']['Movimiento']
            puts m['NroOficialMov']
            Movimiento.create( FechaEfecto:m["FechaEfecto"] , FechaContable:m["FechaContable"] , FechaImpresion:m["FechaImpresion"] , NroEndoso:m["NroEndoso"] , CodMovimientoGral:m["CodMovimientoGral"] , NroRecibo:m["NroRecibo"] , CodTipo:m["CodTipo"] , DescTipoMovimiento:m["DescTipoMovimiento"] , NroOficialMov:m["NroOficialMov"] , NroTramite:m["NroTramite"] , NroMotivoModificacion:m["NroMotivoModificacion"] , DescMotivoModificacion:m["DescMotivoModificacion"] , certificado_id: cert_id)
        end
    else     
        puts 'movimientos array'
        if c['Movimientos']['Movimiento']  then      
            c['Movimientos']['Movimiento'].each do |m|
                puts m.keys
                puts m['NroOficialMov']
                Movimiento.create( FechaEfecto:m["FechaEfecto"] , FechaContable:m["FechaContable"] , FechaImpresion:m["FechaImpresion"] , NroEndoso:m["NroEndoso"] , CodMovimientoGral:m["CodMovimientoGral"] , NroRecibo:m["NroRecibo"] , CodTipo:m["CodTipo"] , DescTipoMovimiento:m["DescTipoMovimiento"] , NroOficialMov:m["NroOficialMov"] , NroTramite:m["NroTramite"] , NroMotivoModificacion:m["NroMotivoModificacion"] , DescMotivoModificacion:m["DescMotivoModificacion"] , certificado_id: cert_id)
            end
        end
    end
end

def anexo(c)
    if c['Anexo'].nil?
        return nil
    end
    a = c['Anexo']
    puts 'anexo'
    puts a['Descripcion']
    ane = Anexo.create(Descripcion: a['Descripcion'], Nota: a['Nota'])
    return ane
end

def certificados(p,pol)
    if  p['Certificados']['Certificado'].class != Array then #es hash
        puts 'cert hash'
        puts 'nro cert #{}'
        c = p['Certificados']['Certificado']
        puts "Acaaaaaaa #{c['NroCertificado']}"
        puts p['codRamo']
        dat,tipo_dat = datosParticularesx(p['codRamo'],c)
        puts "id de datos particulares si #{dat}"
        forma = forma_pagos(c)
        ane = anexo(c)
        cert = Certificado.create(NroCertificado: c['NroCertificado'], VigenciaDesdeC: c['VigenciaDesdeC'], VigenciaHastaC: c['VigenciaHastaC'], FechaEmisionOrig: c['FechaEmisionOrig'], EsPrendaria: c['EsPrendaria'], EsHipotecaria: c['EsHipotecaria'], forma_pago_id: forma, datos_particulares_type: tipo_dat, datos_particulares_id: dat, poliza_id: pol, anexo_id: ane)
        clientes(c,cert.id)
        coverages(c,cert.id)
        ubicacionesRiesgo(c,cert.id)
        recibos(c,cert.id)
        
        movimientos(c,cert.id)
        
    else     
        puts 'cert'     
        a['Certificados']['Certificado'].each do |c| #es arreglo
            puts c['NroCertificado']
            dat,tipo_dat = datosParticularesx(p['codRamo'],c)
            #puts "id de datos particulares si #{dat}"
            forma = forma_pagos(c)
            ane = anexo(c)
            cert = Certificado.create(NroCertificado: c['NroCertificado'], VigenciaDesdeC: c['VigenciaDesdeC'], VigenciaHastaC: c['VigenciaHastaC'], FechaEmisionOrig: c['FechaEmisionOrig'], EsPrendaria: c['EsPrendaria'], EsHipotecaria: c['EsHipotecaria'], forma_pago_id: forma, datos_particulares_type: tipo_dat, datos_particulares_id: dat, poliza_id: pol, anexo_id: ane)
            clientes(c,cert.id)
            coverages(c,cert.id)
            ubicacionesRiesgo(c,cert.id)
            recibos(c,cert.id)
            
            movimientos(c,cert.id)
            
        end
    end
end
def polizas(data_hash)
    i=-1
    data_hash['datos']['Poliza'].each do |p|
        i=i+1
        puts "indice #{i}"
        puts "Poliza #{i}"
        puts "nro"
        puts p['NroOficialGral']
        pol = Poliza.new(TipoPoliza: p['TipoPoliza'], DescTipoPoliza: p['DescTipoPoliza'], NroOficialGral: p['NroOficialGral'], VigenciaDesde: p['VigenciaDesde'], VigenciaHasta: p['VigenciaHasta'], FechaVigenciaInicial: p['FechaVigenciaInicial'], NroReferencia: p['NroReferencia'], NroPropuesta: p['NroPropuesta'], codRamo: p['codRamo'], DescRamo: p['DescRamo'], codFrecuenciaPago: p['codFrecuenciaPago'], DescFrecuenciaPago: p['DescFrecuenciaPago'], CantCuotas: p['CantCuotas'], codTipoFacturacion: p['codTipoFacturacion'], DescTipoFacturacion: p['DescTipoFacturacion'], codProducto: p['codProducto'], DescProducto: p['DescProducto'], codMoneda: p['codMoneda'], DescMoneda: p['DescMoneda'], codOrganizador: p['codOrganizador'], codProductor: p['codProductor'])
        if pol.save then
            puts "poli creada"
        else
           puts pol.errors.full_messages
        end
        certificados(p,pol)
        
        
    end
end

def main
    myXML = File.read("/mnt/c/Users/mferre/Desktop/CosasR/doc1.xml")
    myJSON = Hash.from_xml(myXML).to_json
    data_hash = JSON.parse(myJSON)
    #data_hash['datos']['Poliza'][1]['TipoPoliza']
    polizas(data_hash)
end



#p = Poliza.new(TipoPoliza: a['TipoPoliza'], DescTipoPoliza: a['DescTipoPoliza'], NroOficialGral:a['NroOficialGral'] , VigenciaDesde:a['VigenciaDesde'] , VigenciaHasta: a['VigenciaHasta'], FechaVigenciaInicial: a['FechaVigenciaInicial'], NroReferencia: a['NroReferencia'], NroPropuesta: a['NroPropuesta'], codRamo: a['codRamo'], DescRamo: a['DescRamo'], codFrecuenciaPago: a['codFrecuenciaPago'], DescFrecuenciaPago: a['DescFrecuenciaPago'], CantCuotas: a['CantCuotas'], codTipoFacturacion: a['codTipoFacturacion'], DescTipoFacturacion: a['DescTipoFacturacion'], codProducto: a['codProducto'], DescProducto: a['DescProducto'], codMoneda: a['codMoneda'], DescMoneda: a['DescMoneda'], codOrganizador: a['codOrganizador'], codProductor: a['codProductor'])
#c=Certificado.new(poliza_id=p)certificados(a)