namespace :test_xml do

    task :comenzar => :environment do
        main_poliza
    end
        
    def items(d, tipo_d, id_d)
        def recorrer(it,tipo_d,id_d)
            puts tipo_d
            puts it["CodigoGrupo"]
            Item.create( CodigoGrupo:it["CodigoGrupo"] , TipoBien:it["TipoBien"] , DescripcionBien:it["DescripcionBien"] , Capital:it["Capital"] , Franquicia:it["Franquicia"] , Notas:it["Notas"] , CódigoGrupo:it["CódigoGrupo"] , DescBienOrig:it["DescBienOrig"] , Descripcion:it["Descripcion"] , datos_particulares_type:tipo_d , datos_particulares_id:id_d )  
        end
        if d['DatosPart.'].nil? or d['DatosPart.']['Item'].nil? #MODIFIQUE ESTOOOO
            return nil
        end
        if  d['DatosPart.']['Item'].class != Array then
            puts 'items hash'
            it = d['DatosPart.']['Item']
            recorrer(it,tipo_d,id_d)
            puts 'ITEMS CREADO'
        else     
            puts 'items array'     
            d['DatosPart.']['Item'].each do |it|
                recorrer(it,tipo_d,id_d)
                puts 'ITEMS CREADO'
            end
        end
    end

    def datosParticularesx(codRamo,c,cert_id)
        
        def robos(d)#SeguroIntegral
            if d['Robo'].nil?
                return nil
            end
            r = d['Robo']
            puts 'robo'
            puts r['Zona']
            rob = Robo.create(Zona:r["Zona"] , Actividad:r["Actividad"] , ValorRiesgo:r["ValorRiesgo"] , ModalidadSeguro:r["ModalidadSeguro"] , TipoRiesgo:r["TipoRiesgo"] , Moneda:r["Moneda"])
            if rob then
                return rob.id
            else 
                return nil
            end
        end
        def incendios(d)#seguroIntegral
            if d['Incendio'].nil?
                return nil
            end
            i = d['Incendio']
            puts 'incencio'
            puts i['Actividad']
            inc = Incendio.create(PrimaDeposito: i["PrimaDeposito"], Actividad: i["Actividad"], TipoConstruccion: i["TipoConstruccion"], CantidadPisos: i["CantidadPisos"], TipoRiesgo: i["TipoRiesgo"], TipoCerramiento: i["TipoCerramiento"], PeriodoIndemnizacion: i["PeriodoIndemnizacion"], TipoTecho: i["TipoTecho"], PrimaAnticipo: i["PrimaAnticipo"])
            if inc then
                return inc.id
            else 
                return nil
            end
        end
        def seguroIntegral(c,cert_id)
            def recorrer_seguro_integral(d,cert_id)
                rob_id = robos(d)
                inc_id = incendios(d)
                tipo_dat="DatosParticularesSeguroIntegral"
                dat = DatosParticularesSeguroIntegral.create(TipoSeguro: d["TipoSeguro"], Actividad: d["Actividad"], ZonaRiesgo: d["ZonaRiesgo"], ObjDiversosBienesAsegurados: d["ObjDiversosBienesAsegurados"], robo_id:rob_id , incendio_id:inc_id , certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesSeguroIntegral') and not c['DatosParticularesSeguroIntegral'].nil? then
                if  c['DatosParticularesSeguroIntegral'].class != Array then 
                    d = c['DatosParticularesSeguroIntegral']
                    recorrer_seguro_integral(d,cert_id)
                else 
                    c['DatosParticularesSeguroIntegral'].each do |d|
                        recorrer_seguro_integral(d,cert_id)
                    end
                end
                return "DatosParticularesSeguroIntegral"
            else
                return nil
            end
        end 
        def datosIncendio(c,cert_id)
            def recorrer_datos_incendio(d,cert_id)
                tipo_dat="DatosParticularesIncendio"
                dat = DatosParticularesIncendio.create( Actividad:d["Actividad"] , Detalle:d["Detalle"] , TipoConstruccion:d["TipoConstruccion"] , CantidadPisos:d["CantidadPisos"] , TipoCerramiento:d["TipoCerramiento"] , ImprimirArticulo:d["ImprimirArticulo"] , PrimaDeposito:d["PrimaDeposito"] , LucroCesante:d["LucroCesante"] , TipoTecho:d["TipoTecho"] , PrimaAnticipo:d["PrimaAnticipo"] , certificado_id: cert_id )
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesIncendio') and not c['DatosParticularesIncendio'].nil? then
                if  c['DatosParticularesIncendio'].class != Array then 
                    d = c['DatosParticularesIncendio']
                    recorrer_datos_incendio(d,cert_id)
                else 
                    c['DatosParticularesIncendio'].each do |d|
                        recorrer_datos_incendio(d,cert_id)
                    end
                end
                return "DatosParticularesIncendio"
            else
                return nil
            end
        end 
        def accidentesPersonales(c,cert_id)
            def recorrer_accidentes(d,cert_id)
                tipo_dat="DatosParticularesAccidentesPersonale"
                dat = DatosParticularesAccidentesPersonale.create(NroActividad:d["NroActividad"] , ActividadAsegurado:d["ActividadAsegurado"] , CodClasificacion:d["CodClasificacion"] , Clasificacion:d["Clasificacion"] , Grupo:d["Grupo"] , certificado_id:cert_id  )
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesAccidentesPersonales') and not c['DatosParticularesAccidentesPersonales'].nil? then
                if  c['DatosParticularesAccidentesPersonales'].class != Array then 
                    d = c['DatosParticularesAccidentesPersonales']
                    recorrer_accidentes(d,cert_id)
                else 
                    c['DatosParticularesAccidentesPersonales'].each do |d|
                        recorrer_accidentes(d,cert_id)
                    end
                end
                return "DatosParticularesAccidentesPersonale"
            else
                return nil
            end
        end 
        def combinadoFamiliar(c,cert_id)
            def recorrer_combinado(d,cert_id)
                tipo_dat="DatosParticularesCombinadoFamiliar"
                dat = DatosParticularesCombinadoFamiliar.create( TipoSeguro: d["TipoSeguro"], Actividad: d["Actividad"], ZonaRiesgo: d["ZonaRiesgo"], ObjDiversosBienesAsegurados: d["ObjDiversosBienesAsegurados"], PackVerano: d["PackVerano"], certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesCombinadoFamiliar') and not c['DatosParticularesCombinadoFamiliar'].nil? then
                if  c['DatosParticularesCombinadoFamiliar'].class != Array then 
                    d = c['DatosParticularesCombinadoFamiliar']
                    recorrer_combinado(d,cert_id)
                else 
                    c['DatosParticularesCombinadoFamiliar'].each do |d|
                        recorrer_combinado(d,cert_id)
                    end
                end
                return "DatosParticularesCombinadoFamiliar"
            else
                return nil
            end
        end 
        def vidaColectivo(c,cert_id)
            def recorrer_vida_colectivo(d,cert_id)
                tipo_dat="DatosParticularesVidaColectivo"
                dat = DatosParticularesVidaColectivo.create( EdadReal:d["EdadReal"] , EdadActuarial:d["EdadActuarial"] , CapitalAsegurado:d["CapitalAsegurado"] , Prima:d["Prima"] , DuracionSeguro:d["DuracionSeguro"] , DuracionPagos:d["DuracionPagos"] , EdadMax:d["EdadMax"] , FechaIngTrabajo:d["FechaIngTrabajo"] , Sueldo:d["Sueldo"] , MediaJornada:d["MediaJornada"] , certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesVidaColectivo') and not c['DatosParticularesVidaColectivo'].nil? then
                if  c['DatosParticularesVidaColectivo'].class != Array then 
                    d = c['DatosParticularesVidaColectivo']
                    recorrer_vida_colectivo(d,cert_id)
                else 
                    c['DatosParticularesVidaColectivo'].each do |d|
                        recorrer_vida_colectivo(d,cert_id)
                    end
                end
                return "DatosParticularesVidaColectivo"
            else
                return nil
            end
        end
        def respCivil(c,cert_id)
            def recorrer_resp_civil(d,cert_id)
                tipo_dat="DatosParticularesRespCivil"
                dat = DatosParticularesRespCivil.create( Actividad: d["Actividad"], Capital: d["Capital"], ZonaRiesgo: d["ZonaRiesgo"], Inspeccion: d["Inspeccion"], certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesRespCivil') and not c['DatosParticularesRespCivil'].nil? then
                if  c['DatosParticularesRespCivil'].class != Array then 
                    d = c['DatosParticularesRespCivil']
                    recorrer_resp_civil(d,cert_id)
                else 
                    c['DatosParticularesRespCivil'].each do |d|
                        recorrer_resp_civil(d,cert_id)
                    end
                end
                return "DatosParticularesRespCivil"
            else
                return nil
            end
        end 
        def transporte(c,cert_id)
            def recorrer_transporte(d,cert_id)
                tipo_dat="DatosParticularesTransporte"
                dat = DatosParticularesTransporte.create(LimMaxViaje: d["LimMaxViaje"], Moneda: d["Moneda"], FrecDeclaracion: d["FrecDeclaracion"], TipoMercaderia: d["TipoMercaderia"], MercaderiaInternacional: d["MercaderiaInternacional"], Origen: d["Origen"], Destino: d["Destino"], KmRecorridos: d["KmRecorridos"], certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesTransporte') and not c['DatosParticularesTransporte'].nil? then
                if  c['DatosParticularesTransporte'].class != Array then 
                    d = c['DatosParticularesTransporte']
                    recorrer_transporte(d,cert_id)
                else 
                    c['DatosParticularesTransporte'].each do |d|
                        recorrer_transporte(d,cert_id)
                    end
                end
                return "DatosParticularesTransporte"
            else
                return nil
            end
        end
        def caucion(c,cert_id)
            def recorrer_caucion(d,cert_id)
                tipo_dat="DatosParticularesCaucion"
                dat = DatosParticularesCaucion.create(TipoCaucion: d["TipoCaucion"], AfectaCapital: d["AfectaCapital"], NotaCobertura: d["NotaCobertura"], certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesCaucion') and not c['DatosParticularesCaucion'].nil? then
                if  c['DatosParticularesCaucion'].class != Array then 
                    d = c['DatosParticularesCaucion']
                    recorrer_caucion(d,cert_id)
                else 
                    c['DatosParticularesCaucion'].each do |d|
                        recorrer_caucion(d,cert_id)
                    end
                end
                return "DatosParticularesCaucion"
            else
                return nil
            end
        end
        def seguro_tecnico(c,cert_id)
            def recorrer_seguro_tecnico(d,cert_id)
                tipo_dat="DatosParticularesSeguroTecnico"
                dat = DatosParticularesSeguroTecnico.create(TipoSeguro:d["TipoSeguro"] , Actividad:d["Actividad"] , ZonaRiesgo:d["ZonaRiesgo"] , certificado_id: cert_id)
                items(d,tipo_dat, dat.id)
            end
            if c.has_key?('DatosParticularesSeguroTecnico') and not c['DatosParticularesSeguroTecnico'].nil? then
                if  c['DatosParticularesSeguroTecnico'].class != Array then 
                    d = c['DatosParticularesSeguroTecnico']
                    recorrer_seguro_tecnico(d,cert_id)
                else 
                    c['DatosParticularesSeguroTecnico'].each do |d|
                        recorrer_seguro_tecnico(d,cert_id)
                    end
                end
                return "DatosParticularesSeguroTecnico"
            else
                return nil
            end
            
        end
        
        
        case codRamo
        when '2100'
            puts '2100'
            tipo_dat = seguroIntegral(c,cert_id)
        when '300'
            puts '300'
            tipo_dat= datosIncendio(c,cert_id)
            
        when '600'
            puts '600'
            tipo_dat=accidentesPersonales(c,cert_id) 
            
        when '1800'
            puts '1800'
            tipo_dat = combinadoFamiliar(c,cert_id)
        when '1730'
            puts '1730'
            tipo_dat = vidaColectivo(c,cert_id)
        
        when '1500'
            puts '1500'
            tipo_dat = respCivil(c,cert_id)
        when '500'
            puts '500'
            tipo_dat = transporte(c,cert_id)
        when '1600'
            puts '1600'
            tipo_dat = caucion(c,cert_id)
        when '1400'
            puts '1400'
            tipo_dat = seguro_tecnico(c,cert_id)

        else
            return nil
        end
        return tipo_dat
        
    end

    def telefonos(cli,client_id)
        def recorrer_telefono(t,client_id)
            puts "recorriendo tel"
            Telefono.create( numero: t["Numero"], client_id: client_id)
        end
        puts "ESTOY EN TELEFONOS"
        if cli['Telefonos'].nil?
            return nil
        end
        if  cli['Telefonos']['Telefono'].class != Array then
            puts "tel hash #{cli['Telefonos']['Telefono']}"
            t = cli['Telefonos']['Telefono']
            puts t['Numero']
            recorrer_telefono(t,client_id)
        else     
            puts 'tel array'     
            cli['Telefonos']['Telefono'].each do |t|
                puts t['Numero']
                recorrer_telefono(t,client_id)
            end
        end
    end
    
    
    def clientes(c,cert_id)
        puts "CLIENTES"
       
        def recorrer_client(cli, cert_id)
            client = Client.new(codRol:cli["codRol"] , DescRol:cli["DescRol"] , TipoPersona:cli["TipoPersona"] , ApellidoNombre:cli["ApellidoNombre"] , Direccion:cli["Direccion"] , codCliente:cli["codCliente"] , NroDocumento:cli["NroDocumento"] , codTipoDocumento:cli["codTipoDocumento"] , DescTipoDocumento:cli["DescTipoDocumento"] , NroCUIT:cli["NroCUIT"] , codCondicionIva:cli["codCondicionIva"] , DescCondicionIva:cli["DescCondicionIva"] , CategMonoributo:cli["CategMonoributo"] , DescCategMonotributo:cli["DescCategMonotributo"] , CodPostal:cli["CodPostal"] , codLocalidad:cli["codLocalidad"] , Localidad:cli["Localidad"] , codProvincia:cli["codProvincia"] , Provincia:cli["Provincia"] , email:cli[", email"] , codSexo:cli["codSexo"] , DescSexo:cli["DescSexo"] , FechaNacimiento:cli["FechaNacimiento"] , codEstadoCivil:cli["codEstadoCivil"] , DescEstadoCivil:cli["DescEstadoCivil"] , certificado_id: cert_id)
            if client.save then
                telefonos(cli,client.id)
            else
                puts "ACAAAAAAAAAAA NO ANDUVO"
                puts client.errors.full_messages
            end
        end

        if c['Clientes'].nil?
            return nil
        end
        puts "TIPO DEL CLIENTE"
        puts c['Clientes']['Cliente'].class
        if  c['Clientes']['Cliente'].class != Array then
            puts 'cli hash'
            if c['Clientes']['Cliente'] then 
                cli = c['Clientes']['Cliente']
                puts cli['TipoPersona']
                recorrer_client(cli,cert_id)
            end
        else     
            puts 'cli array'      
            c['Clientes']['Cliente'].each do |cli|
                puts cli['TipoPersona']
                recorrer_client(cli,cert_id)
            end
        end
    end
    def coverages(c,cert_id)
        def recorrer_coverage(cov,cert_id)
            Coverage.create( FechaInicioVigenciaModulo:cov["FechaInicioVigenciaModulo"] , CodModulo:cov["CodModulo"] , DescModulo:cov["DescModulo"] , DescModulo2:cov["DescModulo2"] , FechaInicioVigenciaCobertura:cov["FechaInicioVigenciaCobertura"] , CodCobertura:cov["CodCobertura"] , DescCobertura:cov["DescCobertura"] , SumaAsegurada:cov["SumaAsegurada"] , certificado_id:cert_id)
        end
        if c['Coberturas'].nil?
            return nil
        end
        if  c['Coberturas']['Cobertura'].class != Array then
            puts 'cover hash'
            if c['Coberturas']['Cobertura'] then 
                cov = c['Coberturas']['Cobertura']
                puts cov['CodCobertura']
                recorrer_coverage(cov,cert_id)
            end
        else     
            puts 'cover array'
            if c['Coberturas']['Cobertura'] then      
                c['Coberturas']['Cobertura'].each do |cov|
                    puts cov['CodCobertura']
                    recorrer_coverage(cov,cert_id)
                end
            end
        end
    end

    def ubicacionesRiesgo(c,cert_id)
        def recorrer_ubicacion(u,cert_id)
            UbicacionRiesgo.create(Domicilio: u["Domicilio"], codLocalidad: u["codLocalidad"], Localidad: u["Localidad"], CodPostal: u["CodPostal"], codProvincia: u["codProvincia"], Provincia: u["Provincia"], certificado_id: cert_id)
        end
        if c['UbicacionesRiesgo'].nil?
            return nil
        end
        if  c['UbicacionesRiesgo']['UbicacionRiesgo'].class != Array then
            puts 'ubic hash'
            if c['UbicacionesRiesgo']['UbicacionRiesgo'] then 
                u = c['UbicacionesRiesgo']['UbicacionRiesgo']
                puts u['CodPostal']
                recorrer_ubicacion(u,cert_id)
            end
        else     
            puts 'ubic array'      
            c['UbicacionesRiesgo']['UbicacionRiesgo'].each do |u|
                puts u['CodPostal']
                recorrer_ubicacion(u,cert_id)
            end
        end
    end



    def recibos(c,cert_id)
        def movRecibos(r,rec_id)
            def recorrer_mov(m,rec_id)
                mov = MovRecibo.new( idMovimiento: m["idMovimiento"], codTipoMovimiento: m["codTipoMovimiento"], DescTipoMovimiento: m["DescTipoMovimiento"], FechaContabilizacion: m["FechaContabilizacion"], FechaEfecto: m["FechaEfecto"], FechaCobro: m["FechaCobro"], Importe: m["Importe"], NroTerminal: m["NroTerminal"], NroLote: m["NroLote"], recibo_id: rec_id)
                if not mov.save then
                    puts mov.errors.full_messages
                end
            end
            if r['MovimientosRecibos'].nil?
                return nil
            end
            if  r['MovimientosRecibos']['MovRecibo'].class != Array then
                puts 'movR hash'
                m = r['MovimientosRecibos']['MovRecibo']
                puts m['codTipoMovimiento']
                recorrer_mov(m,rec_id)
            else     
                puts 'MOvR array'     
                r['MovimientosRecibos']['MovRecibo'].each do |m|
                    puts m['codTipoMovimiento']
                    recorrer_mov(m,rec_id)
                end
            end
        end
        def comisiones(r,rec_id)
            if r['Comisiones'].nil?
                return nil
            end
            if  r['Comisiones']['ComisionGenerada'].class != Array then
                puts 'COmision hash'
                co = r['Comisiones']['ComisionGenerada']
                puts "intermed #{co['Intermed']}"
                com = ComisionGenerada.new( Intermed:co["Intermed"] , Monto:co["Monto"] , Porcentaje:co["Porcentaje"] , recibo_id: rec_id)
                if not com.save then 
                    puts com.errors.full_messages
                end
            else     
                puts 'comision array'     
                r['Comisiones']['ComisionGenerada'].each do |co|
                    puts "intermed #{co['Intermed']}"
                    com = ComisionGenerada.create( Intermed:co["Intermed"] , Monto:co["Monto"] , Porcentaje:co["Porcentaje"] , recibo_id: rec_id)
                    if not com.save then 
                        puts com.errors.full_messages
                    end
                end
            end
        end
        if c['Recibos'].nil?
            return nil
        end
        def recorrer_recibo(r,cert_id )
            rec = Recibo.new( NroRecibo: r["NroRecibo"], NroReciboBase: r["NroReciboBase"], VigenciaDesde: r["VigenciaDesde"], VigenciaHasta: r["VigenciaHasta"], FechaVencimiento: r["FechaVencimiento"], NroTransaccion: r["NroTransaccion"], FactorCambio: r["FactorCambio"], ImportePremio: r["ImportePremio"], ImporteSaldo: r["ImporteSaldo"], ImportePrima: r["ImportePrima"], PrimaAnual: r["PrimaAnual"], Periodo: r["Periodo"], PremioAnual: r["PremioAnual"], ImporteIVAGeneral: r["ImporteIVAGeneral"], ImporteDescuento: r["ImporteDescuento"], ImporteTasasImpuestos: r["ImporteTasasImpuestos"], ImporteRedondeo: r["ImporteRedondeo"], ImporteIVAPercepcion: r["ImporteIVAPercepcion"], ImporteIngBrutosPercep: r["ImporteIngBrutosPercep"], ImporteCargoFinanciero: r["ImporteCargoFinanciero"], ImpuestoSellos: r["ImpuestoSellos"], CapitalSocial: r["CapitalSocial"], certificado_id: cert_id)
            if rec.save
                puts "SII"
            else 
                puts rec.errors.full_messages
            end
            movRecibos(r,rec.id)
            comisiones(r,rec.id)
        end
    
        if  c['Recibos']['Recibo'].class != Array then
              puts 'reci hash'
              r = c['Recibos']['Recibo']
              puts r['NroRecibo']
              recorrer_recibo(r,cert_id )
        else     
           puts 'reci array'     
           c['Recibos']['Recibo'].each do |r|
              puts r['NroRecibo']
              recorrer_recibo(r,cert_id )
           end
        end
    end


    def forma_pagos(c)
        puts "Estoy en forma_pagos"
        if c['FormaPago'].nil?
            return nil
        end
        f = c['FormaPago']
        puts 'forma pago'
        puts f['TipoPago']
        forma = FormaPago.create(FechaInicioVigencia:f["FechaInicioVigencia"] , TipoPago:f["TipoPago"] , DescTipoPago:f["DescTipoPago"] , NroCBU:f["NroCBU"] , CodBanco:f["CodBanco"] , codTarjetaCredito:f["codTarjetaCredito"] , DescTarjetaCredito:f["DescTarjetaCredito"] , NroTarjetaCredito:f["NroTarjetaCredito"] , NroCuenta:f["NroCuenta"] )
        return forma.id
    end
    def movimientos(c,cert_id)
        def recorrer_movimiento(m,cert_id)
            Movimiento.create(FechaEfecto: m["FechaEfecto"], FechaContable: m["FechaContable"], FechaImpresion: m["FechaImpresion"], NroEndoso: m["NroEndoso"], CodMovimientoGral: m["CodMovimientoGral"], NroRecibo: m["NroRecibo"], Monto: m["Monto"], CodTipo: m["CodTipo"], DescTipoMovimiento: m["DescTipoMovimiento"], NroOficialMov: m["NroOficialMov"], PolizaAnterior: m["PolizaAnterior"], NroTramite: m["NroTramite"], NroMotivoModificacion: m["NroMotivoModificacion"], DescMotivoModificacion: m["DescMotivoModificacion"], certificado_id: cert_id)    
        end
        if c['Movimientos'].nil?
            return nil
        end
        if  c['Movimientos']['Movimiento'].class != Array then
            puts 'movi hash'
            if c['Movimientos']['Movimiento'] then 
                m = c['Movimientos']['Movimiento']
                recorrer_movimiento(m,cert_id)
            end
        else           
            c['Movimientos']['Movimiento'].each do |m|
                recorrer_movimiento(m,cert_id)
            end
        end
    end

    def anexo(c,cert_id)
        def recorrer_anexo(a,cert_id)
            Anexo.create(Descripcion:a["Descripcion"] , Nota:a["Nota"] , certificado_id: cert_id)    
        end
        if c['Anexo'].nil?
            return nil
        end
        if  c['Anexo'].class != Array then
            puts 'anex hash'
            if c['Anexo'] then 
                a = c['Anexo']
                recorrer_anexo(a,cert_id)
            end
        else           
            c['Anexo'].each do |a|
                recorrer_anexo(a,cert_id)
            end
        end
    end
    def prenda(c)
        puts "Estoy en prenda"
        if c['Prenda'].nil?
            return nil
        end
        p = c['Prenda']
        pren = Prenda.create(FechaContrato:p["FechaContrato"] , FechaEfecto:p["FechaEfecto"] , CantCuotas:p["CantCuotas"] )
        return pren.id
    end

    def accesories(v,vehi_id)
        puts "Estoy en accesories"
        def recorrer_accesories(a,vehi_id)
            Accessory.create( codAccesorio: a["codAccesorio"], DescAccesorio: a["DescAccesorio"], SumaAseguradaAccesorio: a["SumaAseguradaAccesorio"], vehiculo_id: vehi_id)
        end
        if v['Accesorios'].nil?
            return nil
        end
        if  v['Accesorios'].class != Array then
            puts 'acces hash'
            a = v['Accesorios']
            recorrer_accesories(a,vehi_id)
        else           
            v['Accesorios'].each do |a|
                recorrer_accesories(a,vehi_id)
            end
        end
    end
    def vehiculo(c,cert_id)
        puts "Estoy en vehiculo"
        def recorrer_vehiculo(v,cert_id)
            vehi = Vehiculo.create(CodVehiculo: v["CodVehiculo"] , DescVehiculo: v["DescVehiculo"] , NroMotor: v["NroMotor"] , NroChasis: v["NroChasis"] , CodRegistro: v["CodRegistro"] , ModeloAño: v["ModeloAño"] , GuardaEnGarage: v["GuardaEnGarage"] , ConductoresMenoresDe25: v["ConductoresMenoresDe25"] , Hasta25MilKmAnuales: v["Hasta25MilKmAnuales"] , codTipoAsistencia: v["codTipoAsistencia"] , DescTipoAsistencia: v["DescTipoAsistencia"] , codTipoCombustible: v["codTipoCombustible"], DescTipoCombustible: v["DescTipoCombustible"], SumaAsegurada: v["SumaAsegurada"], codUsoVehiculo: v["codUsoVehiculo"], DescUsoVehiculo: v["DescUsoVehiculo"], codClaseVehiculo: v["codClaseVehiculo"], DescClaseVehiculo: v["DescClaseVehiculo"], codMarcaEquipoRastreo: v["codMarcaEquipoRastreo"], DescMarcaEquipoRastreo: v["DescMarcaEquipoRastreo"], ValorGNC: v["ValorGNC"], DescuentoSiniestralidad: v["DescuentoSiniestralidad"], AjusteAutomatico: v["AjusteAutomatico"], MarcaRegulador: v["MarcaRegulador"], NroRegulador: v["NroRegulador"], MarcaCilindro1: v["MarcaCilindro1"], NroCilindro1: v["NroCilindro1"], MarcaCilindro2: v["MarcaCilindro2"], NroCilindro2: v["NroCilindro2"], certificado_id: cert_id)    
            accesories(v,vehi.id)
        end
        if c['Vehiculo'].nil?
            return nil
        end
        if  c['Vehiculo'].class != Array then
            puts 'vehi hash'
            v = c['Vehiculo']
            recorrer_vehiculo(v,cert_id)
        else           
            c['Vehiculo'].each do |u|
                recorrer_vehiculo(v,cert_id)
            end
        end
    end 

   
    def certificados(p,pol_id)
        def crear(p,c,pol_id)
            puts p['codRamo']
            puts "Acaaaaaaa #{c['NroCertificado']}"
            forma_id = forma_pagos(c)
            prenda_id = prenda(c)
            cert = Certificado.create(NroCertificado:c["NroCertificado"] , VigenciaDesdeC:c["VigenciaDesdeC"] , VigenciaHastaC:c["VigenciaHastaC"] , FechaEmisionOrig:c["FechaEmisionOrig"] , FechaAnulacionC:c["FechaAnulacionC"] , CodMotivoAnulacion:c["CodMotivoAnulacion"] , CodCausaAnulacion:c["CodCausaAnulacion"] , EsPrendaria:c["EsPrendaria"] , EsHipotecaria:c["EsHipotecaria"] , prenda_id:prenda_id , forma_pago_id:forma_id , poliza_id:pol_id )
            tipo_dat = datosParticularesx(p['codRamo'],c,cert.id)
            cert.datos_particulares_type = tipo_dat
            cert.save
            clientes(c,cert.id)
            coverages(c,cert.id)
            vehiculo(c,cert.id)
            ubicacionesRiesgo(c,cert.id)
            recibos(c,cert.id)
            movimientos(c,cert.id)
            anexo(c,cert.id)
        end
        if['Certificados'].nil? then 
            return nil 
        end
        if  p['Certificados']['Certificado'].class != Array then #es hash
            puts 'cert hash'
            puts 'nro cert #{}'
            c = p['Certificados']['Certificado']
            crear(p,c,pol_id)
        else     
            puts 'cert'     
            p['Certificados']['Certificado'].each_with_index do |c,i| #es arreglo
                puts "Certificado #{i}"
                crear(p,c,pol_id)
            end
        end
    end
    def polizas(data_hash)
        puts "Estoy en polizas"
        i=-1
        data_hash['datos']['Poliza'].each do |p|
            #p = data_hash["datos"]["Poliza"][1]
            i=i+1
            puts "indice #{i}"
            puts "Poliza #{i}"
            puts "nro"
            puts p['NroOficialGral']
            pol = Poliza.new(TipoPoliza:p["TipoPoliza"] , DescTipoPoliza:p["DescTipoPoliza"] , NroOficialGral:p["NroOficialGral"] , VigenciaDesde:p["VigenciaDesde"] , VigenciaHasta:p["VigenciaHasta"] , FechaVigenciaInicial:p["FechaVigenciaInicial"] , FechaAnulacionPol:p["FechaAnulacionPol"] , NroReferencia:p["NroReferencia"] , NroPropuesta:p["NroPropuesta"] , codRamo:p["codRamo"] , DescRamo:p["DescRamo"] , codFrecuenciaPago:p["codFrecuenciaPago"] , DescFrecuenciaPago:p["DescFrecuenciaPago"] , codGrupoEstadistico:p["codGrupoEstadistico"] , CantCuotas: p["CantCuotas"], codTipoFacturacion:p["codTipoFacturacion"] , DescTipoFacturacion:p["DescTipoFacturacion"] , codProducto:p["codProducto"] , DescProducto:p["DescProducto"] , codMoneda:p["codMoneda"] , DescMoneda:p["DescMoneda"] , codOrganizador:p["codOrganizador"] , codProductor:p["codProductor"] )
            if pol.save then 
                puts "se creo la poliza"
                certificados(p,pol.id)
            else
                puts "error #{pol.errors.full_messages}"
            end
            #certificados(p,pol.id) 
        end
    end

    def main_poliza
        puts "Estoy en main_poliza"
        myXML = File.read("/mnt/c/Users/mferre/Desktop/CosasR/RySNews_20200418_035259.xml")
        myJSON = Hash.from_xml(myXML).to_json
        data_hash = JSON.parse(myJSON)
        #data_hash['datos']['Poliza'][1]['TipoPoliza']
        polizas(data_hash)
    end

end