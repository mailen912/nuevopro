namespace :verificacion do

    task :verificar => :environment do
        main
    end
    
    def telef(c2,tel)
        def recorrer_tel(t,tel)
            t.keys.each do |k|
                tel << k
            end
           
        end
        if c2["Telefonos"].nil?
           return nil
        end
        if c2["Telefonos"]["Telefono"].class != Array
           t = c2["Telefonos"]["Telefono"]
           recorrer_tel(t,tel)
        else
            c2["Telefonos"]["Telefono"].each do |t|
                recorrer_tel(t,tel)
           end
        end
    end

    def clien(c,cli,tel)
        def recorrer_cli(c2,cli,tel)
           c2.keys.each do |k|
               cli << k
           end
           telef(c2,tel)
        end
        if c["Clientes"].nil?
           return nil
        end
        if c["Clientes"]["Cliente"].class != Array
           c2 = c["Clientes"]["Cliente"]
           recorrer_cli(c2,cli,tel)
        else
            c["Clientes"]["Cliente"].each do |c2|
               recorrer_cli(c2,cli,tel)
           end
        end
     end
    def cober(c,cov)
         def recorrer_cov(c3,cov)
             c3.keys.each do |k|
                 cov << k
             end
         end
         if c["Coberturas"].nil?
             return nil
         end
         if c["Coberturas"]["Cobertura"].class != Array
             c3 = c["Coberturas"]["Cobertura"]
             recorrer_cov(c3,cov)
         else
             c["Coberturas"]["Cobertura"].each do |c3|
                 recorrer_cov(c3,cov)
             end
         end
     end
     def ubicacion(c,ubic)
         def recorrer_ubic(u,ubic)
             u.keys.each do |k|
                 ubic << k
             end
         end
         if c['UbicacionesRiesgo'].nil?
             return nil
         end
         if c["UbicacionesRiesgo"]["UbicacionRiesgo"].class != Array
             u = c["UbicacionesRiesgo"]["UbicacionRiesgo"]
             recorrer_ubic(u,ubic)
         else
             c["UbicacionesRiesgo"]["UbicacionRiesgo"].each do |u|
                 recorrer_ubic(u,ubic)
             end
         end
     end
    def mrecib(r,mrec)
       def recorrer_mrecib(m,mrec)
           m.keys.each do |k|
                 mrec << k
             end
         end
         if r['MovimientosRecibos'].nil?
             return nil
         end
         if r["MovimientosRecibos"]["MovRecibo"].class != Array
             m = r["MovimientosRecibos"]["MovRecibo"]
             recorrer_mrecib(m,mrec)
         else
            r["MovimientosRecibos"]["MovRecibo"].each do |m|
            recorrer_mrecib(m,mrec)
            end
         end
     end
     def comis(r,com)
        def recorrer_comis(c,com)
            c.keys.each do |k|
                com << k
            end
        end
        if r["Comisiones"].nil?    
             return nil
        end
        if r["Comisiones"]["ComisionGenerada"].class != Array
            c = r["Comisiones"]["ComisionGenerada"]
            recorrer_comis(c,com)
        else
            r["Comisiones"]["ComisionGenerada"].each do |c|
                recorrer_comis(c,com)
            end
        end
    end

    def recib(c,rec,mrec,com)
        def recorrer_rec(r,rec,mrec,com)
            r.keys.each do |k|
                rec << k
            end
            if not r["MovimientosRecibos"].nil?
                mrecib(r,mrec)
            end
            if not r["Comisiones"].nil?
                comis(r,com)
           end
        end
        if c['Recibos'].nil?
            return nil
        end
        if c["Recibos"]["Recibo"].class != Array
            r = c["Recibos"]["Recibo"]
            recorrer_rec(r,rec,mrec,com)
        else
            c["Recibos"]["Recibo"].each do |r|
                recorrer_rec(r,rec,mrec,com)
            end
        end
    end
    def forma_p(c,form)
        if  c['FormaPago'].nil?
            return nil
        end
        f = c["FormaPago"]
        f.keys.each do |k|
            form << k
        end
    end

    def movimient(c,mov)
         def recorrer_movi(u,ubic)
            u.keys.each do |k|
                ubic << k
            end
        end
        if c['Movimientos'].nil?
            return nil
        end
        if c["Movimientos"]["Movimiento"].class != Array
           m = c["Movimientos"]["Movimiento"]
            recorrer_movi(m,mov)
        else
             c["Movimientos"]["Movimiento"].each do |m|
                recorrer_movi(m,mov)
            end
        end
    end
    def prend(c,pren)
        if c['Prenda'].nil?
            return nil
        end
        p = c["Prenda"]
        p.keys.each do |k|
            pren << k
        end
    end
    def accesor(v,acces)
        def recorrer_acces(a,acces)
            a.keys.each do |k|
                acces << k
            end
        end
        if v['Accesorios'].nil?
            return nil
        end
        
        if v["Accesorios"].class != Array
            a = v["Accesorios"]
            recorrer_acces(a,acces)
        else
            v["Accesorios"].each do |a|
                recorrer_acces(a,acces)
            end
        end
    end

    def vehic(c,vehi,acces)
        if c['Vehiculo'].nil?
            return nil
        end
        v = c["Vehiculo"]
        v.keys.each do |k|
            vehi << k
        end
        accesor(v,acces)
    end
    def robi(si,rob)
        if  si['Robo'].nil?
            return nil
        end
        r = si["Robo"]
        r.keys.each do |k|
            rob << k
        end
    end
    def incend(si,inc)
        if si['Incendio'].nil?
            return nil
        end
        i = si["Incendio"]
        i.keys.each do |k|
            inc << k
        end
    end

    def ite(d,it)
        def recorrer_item(i,it)
            i.keys.each do |k|
                it << k
            end
        end
        if d["DatosPart."].nil?
            return nil
        end
        if d["DatosPart."]["Item"].class != Array
            i = d["DatosPart."]["Item"]
            recorrer_item(i,it)
        else
            d["DatosPart."]["Item"].each do |i|
                recorrer_item(i,it)
            end
        end

    end

    def d_p_seg_int(c,seg_int,rob,inc,it)
        if c['DatosParticularesSeguroIntegral'].nil?
            return nil
        end
        si = c["DatosParticularesSeguroIntegral"]
        si.keys.each do |k|
            seg_int << k
        end
        robi(si,rob)
        incend(si,inc)
        ite(si,it)
    end

    def d_p_inc(c,d_inc,it)
        if  c['DatosParticularesIncendio'].nil?
            return nil
        end
        i = c["DatosParticularesIncendio"]
        i.keys.each do |k|
            d_inc << k
        end
        ite(i,it)
    end

    def d_p_acc_p(c,acc_p,it)
        if c['DatosParticularesAccidentesPersonales'].nil?
            return nil
        end
        a = c["DatosParticularesAccidentesPersonales"]
        a.keys.each do |k|
            acc_p << k
        end
        ite(a,it)
    end
    def d_p_comb_f(c, comb,it)
        if c['DatosParticularesCombinadoFamiliar'].nil?
            return nil
        end
        cf = c["DatosParticularesCombinadoFamiliar"]
        cf.keys.each do |k|
            comb << k
        end
        ite(cf,it)
    end

    def d_p_cau(c, cau,it)
        if c['DatosParticularesCaucion'].nil?
            return nil
        end
        cc = c["DatosParticularesCaucion"]
        cc.keys.each do |k|
            cau << k
        end
        ite(cc,it)
    end
    def d_p_vida(c, vida,it)
        def recorrer_vid(v,vida, it)
            v.keys.each do |k|
                vida << k
            end
            ite(v,it)
        end
        if c['DatosParticularesVidaColectivo'].nil?
            return nil
        end
        if c["DatosParticularesVidaColectivo"].class != Array
            v = c["DatosParticularesVidaColectivo"]
            recorrer_vid(v,vida, it)
        else
            c["DatosParticularesVidaColectivo"].each do |v|
                recorrer_vid(v,vida, it)
            end
        end
    end
    def d_p_resp(c, resp,it)
        if c['DatosParticularesRespCivil'].nil?
            return nil
        end
        r = c["DatosParticularesRespCivil"]
        r.keys.each do |k|
            resp << k
        end
        ite(r,it)
    end
    def d_p_seg_tec(c, seg_tec,it)
        if c['DatosParticularesSeguroTecnico'].nil?
            return nil
        end
        cf = c["DatosParticularesSeguroTecnico"]
        cf.keys.each do |k|
            seg_tec << k
        end
        ite(cf,it)
    end

    def d_p_trans(c, tran,it)
        if c['DatosParticularesTransporte'].nil?
            return nil
        end
        t = c["DatosParticularesTransporte"]
        t.keys.each do |k|
            tran << k
        end
        ite(t,it)
    end

    def anex(c,ane)
        def recorrer_ane(a,ane)
            a.keys.each do |k|
                ane << k
            end
        end
        if c['Anexo'].nil?
            return nil
        end
        if c["Anexo"].class != Array
            a = c["Anexo"]
            recorrer_ane(a,ane)
        else
            c["Anexo"].each do |a|
                recorrer_ane(a,ane)
            end
        end
    end
  

    def certif(p,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob,inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,it, tran, ane, cant)
        def recorrer_cert(c,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob, inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,tran,it,ane)
            c.keys.each do |k|
                cert << k
            end
            clien(c,cli,tel)    
            cober(c,cov)
            ubicacion(c,ubic)
            recib(c,rec,mrec,com)
            forma_p(c,form)
            movimient(c,mov)
            prend(c,pren)
            vehic(c,vehi,acces)
            d_p_seg_int(c,seg_int,rob,inc,it)
            d_p_inc(c,d_inc,it)
            d_p_acc_p(c,acc_p,it)
            d_p_comb_f(c, comb,it)
            d_p_cau(c, cau,it)
            d_p_vida(c, vida,it)
            d_p_resp(c, resp,it)
            d_p_seg_tec(c, seg_tec,it)
            d_p_trans(c, tran,it)
            anex(c,ane)
        end
        if p["Certificados"].nil?
            return nil
        end
        if p["Certificados"]["Certificado"].class != Array
            c = p["Certificados"]["Certificado"]
            recorrer_cert(c,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob, inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,tran,it,ane)
            cant=cant+1
        else
            p["Certificados"]["Certificado"].each do |c|
                recorrer_cert(c,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob, inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,tran,it,ane)
                cant=cant+1
            end
        end
        cant
    end

    def main
        pol,cli,cov =Set.new, Set.new, Set.new
        tel = Set.new
        cert = Set.new
        ubic = Set.new
        rec = Set.new
        mrec = Set.new
        com = Set.new
        form = Set.new
        mov = Set.new
        pren = Set.new
        vehi = Set.new
        acces = Set.new
        seg_int, d_inc, acc_p, comb, cau, vida, resp,seg_tec,tran = Set.new, Set.new, Set.new, Set.new,Set.new, Set.new, Set.new, Set.new, Set.new
        rob = Set.new
        inc = Set.new
        it = Set.new
        ane = Set.new
        cant=0
        
        puts "verificando"
        myXML = File.read("/mnt/c/Users/mferre/Desktop/CosasR/RySNews_20200421_034701.xml")
        myJSON = Hash.from_xml(myXML).to_json
        file = JSON.parse(myJSON)
        file["datos"]["Poliza"].each do |p|
            p.keys.each do |k|
                pol << k
            end
            cant = certif(p,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob,inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,it, tran, ane, cant)
        end
        myXML2 = File.read("/mnt/c/Users/mferre/Desktop/CosasR/RySNews_20200423_034917.xml")
        myJSON2 = Hash.from_xml(myXML2).to_json
        file2 = JSON.parse(myJSON2)
        file2["datos"]["Poliza"].each do |p|
            p.keys.each do |k|
                pol << k
            end
            cant = certif(p,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob,inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,it, tran, ane, cant)
        end
        myXML3 = File.read("/mnt/c/Users/mferre/Desktop/CosasR/RySNews_20200418_035259.xml")
        myJSON3 = Hash.from_xml(myXML3).to_json
        file3 = JSON.parse(myJSON3)
        file3["datos"]["Poliza"].each do |p|
            p.keys.each do |k|
                pol << k
            end
            cant = certif(p,cert,cli,tel,cov,ubic,rec,mrec,com,form,mov,pren,vehi,acces,seg_int,rob,inc, d_inc, acc_p, comb,cau, vida, resp,seg_tec,it, tran, ane, cant)
        end
        puntos = "----------------------------------------------------------------------------------------------------------------------------"
        puts "cant"
        puts cant
        puts "pol #{pol.size}"
        puts "cert #{cert.size}"
        puts "cli #{cli.size}"
        puts "cli #{tel.size}"
        puts "cov #{cov.size}" 
        puts "ubic #{ubic.size}"
        puts "rec #{rec.size}" 
        puts "mrec #{mrec.size}" 
        puts "com #{com.size}" 
        puts "form #{form.size}" 
        puts "mov #{mov.size}" 
        puts "pren #{pren.size}"
        puts "vehi #{vehi.size}"  
        puts "acces #{acces.size}"  
        puts "seg_int #{seg_int.size}"  
        puts "rob #{rob.size}"  
        puts "inc #{inc.size}"  
        puts "d_inc #{d_inc.size}"  
        puts "acc_p #{acc_p.size}" 
        puts "comb #{comb.size}"
        puts "cau #{cau.size}"
        puts "vida #{vida.size}"  
        puts "resp #{resp.size}"
        puts "seg_tec #{seg_tec.size}"
        puts "tran #{tran.size}"
        puts "it #{it.size}"
        puts "ane #{ane.size}"
        puts "POLIZA"
        pol.each do |e|
            puts e 
        end
        puts puntos
        puts "CERTIFICADO"
        cert.each do |e|
            puts e 
        end
        puts puntos 
        puts "CLIENTES"
        cli.each do |e|
            puts e
        end
        puts puntos 
        puts "TELEFONOS"
        tel.each do |e|
            puts e
        end
        puts puntos 
        puts "COBERTURA"
        cov.each do |e|
            puts e 
        end
        puts puntos 
        puts "UBICACION"
        ubic.each do |e|
            puts e 
        end
        puts puntos 
        puts "RECIBO"
        rec.each do |e|
            puts e 
        end
        puts puntos 
        puts "MOV RECIBO"
        mrec.each do |e|
            puts e 
        end
        puts puntos 
        puts "COMISION"
        com.each do |e|
            puts e 
        end
        puts puntos 
        puts "FORMA DE PAGO"
        form.each do |e|
            puts e 
        end
        puts puntos 
        puts "MOVIMIENTO"
        mov.each do |e|
            puts e 
        end
        puts puntos 
        puts "PRENDA"
        pren.each do |e|
            puts e 
        end
        puts puntos 
        puts "VEHICULO"
        vehi.each do |e|
            puts e 
        end
        puts puntos 
        puts "ACCESORIOS"
        acces.each do |e|
            puts e 
        end
        puts puntos 
        puts "SEGUROINTEGRAL"
        seg_int.each do |e|
            puts e 
        end
        puts puntos 
        puts "ROBO"
        rob.each do |e|
            puts e 
        end
        puts puntos 
        puts "INCENDIO"
        inc.each do |e|
            puts e 
        end
        puts puntos 
        puts "DATOS INCENDIO"
        d_inc.each do |e|
            puts e 
        end
        puts puntos 
        puts "ACCIDENTES PERSONALES"
        acc_p.each do |e|
            puts e 
        end
        puts puntos 
        puts "COMBINADO FAMILIAR"
        comb.each do |e|
            puts e 
        end
        puts puntos 
        puts "CAUCION"
        cau.each do |e|
            puts e 
        end
        puts puntos 
        puts "VIDA COMB"
        vida.each do |e|
            puts e 
        end
        puts puntos 
        puts "RESP"
        resp.each do |e|
            puts e 
        end
        puts puntos 
        puts "SEG TEC"
        seg_tec.each do |e|
            puts e 
        end
        puts puntos 
        puts "TRANSPORTE"
        tran.each do |e|
            puts e 
        end
        puts puntos 
        puts "ITEM"
        it.each do |e|
            puts e 
        end
        puts puntos 
        puts "ANEXO"
        ane.each do |e|
            puts e 
        end
    end
end

#haciendo accidentes personales