class Certificado < ApplicationRecord
    
    has_many :client
    has_many :coverage
    has_many :ubicacion_riesgo
    has_many :recibo
    references :forma_pago
    has_many :movimiento
    belongs_to :poliza
    references :anexo

end
