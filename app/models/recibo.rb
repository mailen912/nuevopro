class Recibo < ApplicationRecord
    has_many :mov_recibo
    has_many :comision_generada
    belongs_to :certificado
end
