class CreateMovRecibos < ActiveRecord::Migration[6.1]
  def change
    create_table :mov_recibos do |t|
      t.bigint :idMovimiento
      t.integer :codTipoMovimiento
      t.string :DescTipoMovimiento
      t.datetime :FechaContabilizacion
      t.datetime :FechaEfecto
      t.datetime :FechaCobro
      t.float :Importe
      t.integer :NroTerminal
      t.integer :NroLote
      t.belongs_to :recibo
      t.timestamps
    end
  end
end
