class CreateTelefonos < ActiveRecord::Migration[6.1]
  def change
    create_table :telefonos do |t|
      t.string :numero
      t.belongs_to :client
      t.timestamps
    end
  end
end
