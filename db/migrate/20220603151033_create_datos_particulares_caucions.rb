class CreateDatosParticularesCaucions < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_caucions do |t|
      t.string :TipoCaucion
      t.string :AfectaCapital
      t.string :NotaCobertura
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
