class CreateDatosParticularesTransportes < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_transportes do |t|
      t.float :LimMaxViaje
      t.string :Moneda
      t.string :FrecDeclaracion
      t.string :TipoMercaderia
      t.string :MercaderiaInternacional
      t.string :Origen
      t.string :Destino
      t.float :KmRecorridos
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
