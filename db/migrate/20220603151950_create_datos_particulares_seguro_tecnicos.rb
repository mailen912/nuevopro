class CreateDatosParticularesSeguroTecnicos < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_seguro_tecnicos do |t|
      t.string :TipoSeguro
      t.string :Actividad
      t.string :ZonaRiesgo
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
