class CreateDatosParticularesRespCivils < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_resp_civils do |t|
      t.string :Actividad
      t.float :Capital
      t.string :ZonaRiesgo
      t.string :Inspeccion
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
