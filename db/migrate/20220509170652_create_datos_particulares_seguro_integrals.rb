class CreateDatosParticularesSeguroIntegrals < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_seguro_integrals do |t|
      t.string :TipoSeguro
      t.string :Actividad
      t.string :ZonaRiesgo
      t.string :ObjDiversosBienesAsegurados
      t.references :robo
      t.references :incendio
      t.belongs_to :certificado

      t.timestamps
    end
  end
end
