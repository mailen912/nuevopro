class CreateCertificados < ActiveRecord::Migration[6.1]
  def change
    create_table :certificados do |t|
      t.integer :NroCertificado
      t.datetime :VigenciaDesdeC
      t.datetime :VigenciaHastaC
      t.datetime :FechaEmisionOrig
      t.datetime :FechaAnulacionC
      t.integer :CodMotivoAnulacion
      t.integer :CodCausaAnulacion
      t.boolean :EsPrendaria
      t.boolean :EsHipotecaria
      t.references :prenda
      t.string :datos_particulares_type
      t.references :forma_pago
      t.belongs_to :poliza


      t.timestamps
    end
  end
end
