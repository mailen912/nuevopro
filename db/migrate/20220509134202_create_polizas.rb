class CreatePolizas < ActiveRecord::Migration[6.1]
  def change
    #faltan PolizasCombinadas
    create_table :polizas do |t|
      t.integer :TipoPoliza
      t.string :DescTipoPoliza
      t.bigint :NroOficialGral
      t.datetime :VigenciaDesde
      t.datetime :VigenciaHasta
      t.datetime :FechaVigenciaInicial
      t.datetime :FechaAnulacionPol
      t.bigint :NroReferencia
      t.string :NroPropuesta
      t.integer :codRamo
      t.string :DescRamo
      t.integer :codFrecuenciaPago
      t.string :DescFrecuenciaPago
      t.integer :codGrupoEstadistico
      t.integer :CantCuotas
      t.integer :codTipoFacturacion
      t.string :DescTipoFacturacion
      t.integer :codProducto
      t.string :DescProducto
      t.integer :codMoneda
      t.string :DescMoneda
      t.bigint :codOrganizador
      t.bigint :codProductor

      t.timestamps
    end
  end
end
