class CreateDatosParticularesAccidentesPersonales < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_accidentes_personales do |t|
      t.integer :NroActividad
      t.string :ActividadAsegurado
      t.integer :CodClasificacion
      t.string :Clasificacion
      t.integer :Grupo
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
