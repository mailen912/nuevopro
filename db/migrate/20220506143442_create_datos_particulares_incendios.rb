class CreateDatosParticularesIncendios < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_incendios do |t|
      t.string :Actividad
      t.string :Detalle
      t.string :TipoConstruccion
      t.integer :CantidadPisos
      t.string :TipoCerramiento
      t.string :ImprimirArticulo
      t.float :PrimaDeposito
      t.float :LucroCesante
      t.string :TipoTecho
      t.float :PrimaAnticipo
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
