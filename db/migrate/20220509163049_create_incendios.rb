class CreateIncendios < ActiveRecord::Migration[6.1]
  def change
    create_table :incendios do |t|
      t.float :PrimaDeposito
      t.string :Actividad
      t.string :TipoConstruccion
      t.integer :CantidadPisos
      t.string :TipoRiesgo
      t.string :TipoCerramiento
      t.integer :PeriodoIndemnizacion
      t.string :TipoTecho
      t.float :PrimaAnticipo

      t.timestamps
    end
  end
end
