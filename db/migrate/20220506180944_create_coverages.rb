class CreateCoverages < ActiveRecord::Migration[6.1]
  def change
    create_table :coverages do |t|
      t.datetime :FechaInicioVigenciaModulo
      t.integer :CodModulo
      t.string :DescModulo
      t.string :DescModulo2
      t.datetime :FechaInicioVigenciaCobertura
      t.integer :CodCobertura
      t.string :DescCobertura
      t.float :SumaAsegurada
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
