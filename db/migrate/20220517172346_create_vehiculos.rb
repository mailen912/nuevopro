class CreateVehiculos < ActiveRecord::Migration[6.1]
  def change
    create_table :vehiculos do |t|
      t.string :CodVehiculo
      t.string :DescVehiculo
      t.string :NroMotor
      t.string :NroChasis
      t.string :CodRegistro
      t.integer :ModeloAño
      t.boolean :GuardaEnGarage
      t.boolean :ConductoresMenoresDe25
      t.boolean :Hasta25MilKmAnuales
      t.integer :codTipoAsistencia
      t.string :DescTipoAsistencia
      t.integer :codTipoCombustible
      t.string :DescTipoCombustible
      t.float :SumaAsegurada
      t.integer :codUsoVehiculo
      t.string :DescUsoVehiculo
      t.integer :codClaseVehiculo
      t.string :DescClaseVehiculo
      t.integer :codMarcaEquipoRastreo
      t.string :DescMarcaEquipoRastreo
      t.float :ValorGNC
      t.float :DescuentoSiniestralidad
      t.float :AjusteAutomatico
      t.string :MarcaRegulador
      t.string :NroRegulador
      t.string :MarcaCilindro1
      t.string :NroCilindro1
      t.string :MarcaCilindro2
      t.string :NroCilindro2
      
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
