class CreateAnexos < ActiveRecord::Migration[6.1]
  def change
    create_table :anexos do |t|
      t.string :Descripcion
      t.text :Nota
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
