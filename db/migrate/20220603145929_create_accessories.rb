class CreateAccessories < ActiveRecord::Migration[6.1]
  def change
    create_table :accessories do |t|
      t.integer :codAccesorio
      t.string :DescAccesorio
      t.float :SumaAseguradaAccesorio
      t.belongs_to :vehiculo
      t.timestamps
    end
  end
end
