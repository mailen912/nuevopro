class CreateComisionGeneradas < ActiveRecord::Migration[6.1]
  def change
    create_table :comision_generadas do |t|
      t.bigint :Intermed
      t.float :Monto
      t.float :Porcentaje
      t.belongs_to :recibo
      t.timestamps
    end
  end
end
