class CreateDatosParticularesCombinadoFamiliars < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_combinado_familiars do |t|
      t.string :TipoSeguro
      t.string :Actividad
      t.string :ZonaRiesgo
      t.string :ObjDiversosBienesAsegurados
      t.float :PackVerano
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
