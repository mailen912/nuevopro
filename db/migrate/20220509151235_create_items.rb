class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :items do |t|
      t.string :CodigoGrupo
      t.string :TipoBien
      t.string :DescripcionBien
      t.float :Capital
      t.float :Franquicia
      t.string :Notas
      t.integer :CódigoGrupo
      t.string :DescBienOrig
      t.string :Descripcion
      t.belongs_to :datos_particulares, polymorphic: true
      t.timestamps
    end
  end
end
