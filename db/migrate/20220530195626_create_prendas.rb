class CreatePrendas < ActiveRecord::Migration[6.1]
  def change
    create_table :prendas do |t|
      t.datetime :FechaContrato
      t.datetime :FechaEfecto
      t.integer :CantCuotas

      t.timestamps
    end
  end
end
