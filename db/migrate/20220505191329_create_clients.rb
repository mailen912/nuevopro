class CreateClients < ActiveRecord::Migration[6.1]
  def change
    create_table :clients do |t|
      t.integer :codRol
      t.string :DescRol
      t.string :TipoPersona
      t.string :ApellidoNombre
      t.string :Direccion
      t.string :codCliente
      t.bigint :NroDocumento
      t.string :codTipoDocumento
      t.string :DescTipoDocumento
      t.string :NroCUIT
      t.string :codCondicionIva
      t.string :DescCondicionIva
      t.string :CategMonoributo
      t.string :DescCategMonotributo
      t.integer :CodPostal
      t.integer :codLocalidad
      t.string :Localidad
      t.integer :codProvincia
      t.string :Provincia
      t.string :email
      t.integer :codSexo
      t.string :DescSexo
      t.datetime :FechaNacimiento
      t.integer :codEstadoCivil
      t.string :DescEstadoCivil
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
