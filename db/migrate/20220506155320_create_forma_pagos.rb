class CreateFormaPagos < ActiveRecord::Migration[6.1]
  def change
    create_table :forma_pagos do |t|
      t.datetime :FechaInicioVigencia
      t.integer :TipoPago
      t.string :DescTipoPago
      t.string :NroCBU
      t.integer :CodBanco
      t.integer :codTarjetaCredito
      t.string :DescTarjetaCredito
      t.string :NroTarjetaCredito
      t.bigint :NroCuenta

      t.timestamps
    end
  end
end
