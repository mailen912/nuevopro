class CreateUbicacionRiesgos < ActiveRecord::Migration[6.1]
  def change
    create_table :ubicacion_riesgos do |t|
      t.string :Domicilio
      t.integer :codLocalidad
      t.string :Localidad
      t.integer :CodPostal
      t.integer :codProvincia
      t.string :Provincia
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
