class CreateRobos < ActiveRecord::Migration[6.1]
  def change
    create_table :robos do |t|
      t.string :Zona
      t.string :Actividad
      t.float :ValorRiesgo
      t.string :ModalidadSeguro
      t.string :TipoRiesgo
      t.string :Moneda

      t.timestamps
    end
  end
end
