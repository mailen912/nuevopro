class CreateRecibos < ActiveRecord::Migration[6.1]
  def change
    create_table :recibos do |t|
      t.bigint :NroRecibo
      t.bigint :NroReciboBase
      t.datetime :VigenciaDesde
      t.datetime :VigenciaHasta
      t.datetime :FechaVencimiento
      t.integer :NroTransaccion
      t.float :FactorCambio
      t.float :ImportePremio
      t.float :ImporteSaldo
      t.float :ImportePrima
      t.float :PrimaAnual
      t.integer :Periodo
      t.float :PremioAnual
      t.float :ImporteIVAGeneral
      t.float :ImporteDescuento
      t.float :ImporteTasasImpuestos
      t.float :ImporteRedondeo
      t.float :ImporteIVAPercepcion
      t.float :ImporteIngBrutosPercep
      t.float :ImporteCargoFinanciero
      t.float :ImpuestoSellos
      t.float :CapitalSocial
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
