class CreateDatosParticularesVidaColectivos < ActiveRecord::Migration[6.1]
  def change
    create_table :datos_particulares_vida_colectivos do |t|
      t.integer :EdadReal
      t.integer :EdadActuarial
      t.float :CapitalAsegurado
      t.float :Prima
      t.string :DuracionSeguro
      t.string :DuracionPagos
      t.integer :EdadMax
      t.datetime :FechaIngTrabajo
      t.float :Sueldo
      t.string :MediaJornada
      t.belongs_to :certificado
      t.timestamps
    end
  end
end
