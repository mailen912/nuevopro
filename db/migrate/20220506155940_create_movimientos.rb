class CreateMovimientos < ActiveRecord::Migration[6.1]
  def change
    create_table :movimientos do |t|
      t.datetime :FechaEfecto
      t.datetime :FechaContable
      t.datetime :FechaImpresion
      t.integer :NroEndoso
      t.integer :CodMovimientoGral
      t.integer :NroRecibo
      t.float :Monto
      t.integer :CodTipo
      t.string :DescTipoMovimiento
      t.integer :NroOficialMov
      t.bigint :PolizaAnterior
      t.string :NroTramite
      t.integer :NroMotivoModificacion
      t.string :DescMotivoModificacion
      
      t.belongs_to :certificado
      
      t.timestamps
    end
  end
end
