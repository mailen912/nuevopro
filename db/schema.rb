# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_03_151950) do

  create_table "accesorios", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id"
    t.integer "accesorio_id", null: false
    t.integer "amount", null: false
  end

  create_table "accessories", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "codAccesorio"
    t.string "DescAccesorio"
    t.float "SumaAseguradaAccesorio"
    t.bigint "vehiculo_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["vehiculo_id"], name: "index_accessories_on_vehiculo_id"
  end

  create_table "addresses", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "client_id", null: false
    t.string "calle", limit: 75, null: false
    t.string "numero", limit: 15
    t.string "piso", limit: 10
    t.string "dpto", limit: 10
    t.string "telefono", limit: 50
    t.string "codigo_postal", limit: 10, null: false
    t.string "localidad", limit: 100
    t.integer "provincia", limit: 2
    t.datetime "creation_date", null: false
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "administradores_consorcios", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 150, null: false
    t.string "telephone", limit: 20
    t.string "mail", limit: 75
    t.integer "zip_code", limit: 2
    t.string "city", limit: 75
    t.integer "state", limit: 2
    t.string "address", limit: 100
    t.integer "producer_id", null: false
    t.integer "seller_id", null: false
  end

  create_table "anexos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "Descripcion"
    t.text "Nota"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_anexos_on_certificado_id"
  end

  create_table "apikey_token", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 75, null: false
    t.integer "id_producer", null: false
    t.integer "seller_id"
    t.integer "id_multicotizador", null: false
    t.string "apikey", limit: 200, null: false
    t.string "site_url", limit: 50, null: false
    t.string "json_url", limit: 50, null: false
    t.datetime "creation_date"
    t.index ["id"], name: "id", unique: true
  end

  create_table "automatic_adjustment", id: { type: :integer, limit: 2, default: nil }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "adjustment", limit: 2, null: false
  end

  create_table "beneficiarios_nomina", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_nomina", null: false
    t.string "name", limit: 75, null: false
    t.string "dni", limit: 10, null: false
    t.integer "percent", limit: 2, default: 1, null: false
    t.string "mail", limit: 100
    t.string "telephone", limit: 20
  end

  create_table "cartera_cascos", id: :integer, default: nil, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "producer_id", null: false
    t.string "matricula", limit: 200
    t.string "registro", limit: 200
    t.string "name", limit: 200
    t.string "marca", limit: 200
    t.string "modelo", limit: 200
    t.string "nro_serie", limit: 200
    t.integer "anio"
    t.string "uso", limit: 100
    t.string "cant_plazas", limit: 100
    t.string "cant_tripulantes", limit: 100
    t.string "cant_pasajeros", limit: 100
    t.string "peso_max", limit: 100
    t.date "adquisicion_date"
    t.integer "cod_cli"
  end

  create_table "cartera_cuotas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "cartera_id", null: false
    t.string "nro", limit: 3, null: false
    t.date "expiration_date", null: false
    t.decimal "amount", precision: 10, scale: 2, null: false
    t.integer "status", limit: 2, null: false
  end

  create_table "cartera_maquinaria", id: :integer, default: nil, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "type_name", limit: 250
    t.string "model", limit: 250
    t.string "year", limit: 5
    t.string "serial_number", limit: 250
    t.string "manufacturer", limit: 250
    t.string "motor", limit: 250
    t.integer "producer_id", null: false
  end

  create_table "cartera_nominas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "nombre", limit: 250, null: false
    t.string "tipo_dni", limit: 3, default: "96", null: false
    t.string "dni", limit: 13
    t.date "birthdate"
    t.integer "producer_id", null: false
  end

  create_table "cartera_otros_riesgos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "cartera_id", null: false
    t.text "riesgo"
    t.integer "producer_id", null: false
  end

  create_table "cartera_riesgos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "cartera_id", null: false
    t.integer "riesgo_id", null: false
    t.integer "riesgo_type", limit: 2, null: false, comment: "1-auto|2-nomina|3-ubicacion|4-maquinaria"
    t.string "product_description", limit: 500
    t.text "coberturas"
    t.string "franquicia", limit: 250
    t.text "bonificaciones"
    t.text "herederos"
    t.decimal "amount", precision: 10, scale: 2
  end

  create_table "cartera_ubicaciones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "address", limit: 500, null: false
    t.string "city", limit: 250
    t.string "zip_code", limit: 10
    t.integer "state_id", limit: 2
    t.string "activity_code", limit: 30
    t.string "activity", limit: 300
    t.integer "producer_id", null: false
  end

  create_table "cartera_vigente", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "producer_id", null: false
    t.integer "client_id", null: false
    t.string "client_type", limit: 1, default: "F", null: false
    t.integer "ramo_id", limit: 2, default: 1, null: false
    t.string "nro_poliza", limit: 50, null: false
    t.string "nro_int_cia", limit: 50, null: false
    t.integer "endoso", default: 0, null: false
    t.string "nro_poliza_anterior", limit: 50
    t.integer "status_id", limit: 2, default: 1, null: false
    t.integer "movimiento", limit: 2
    t.string "description", limit: 200
    t.date "creation_date", null: false
    t.date "vigencia_from", null: false
    t.date "vigencia_to"
    t.decimal "prima", precision: 10, scale: 2
    t.decimal "extra_prima", precision: 10, scale: 2
    t.decimal "premio", precision: 10, scale: 2
    t.decimal "impuestos", precision: 10, scale: 2
    t.decimal "recargo_financiero", precision: 10, scale: 2
    t.integer "payway_id", limit: 2
    t.string "facturacion_id", limit: 2
    t.integer "money_type", limit: 2, default: 1, null: false
    t.string "condicion_iva", limit: 5, default: "CF", null: false
    t.integer "company_id", limit: 2, null: false
    t.datetime "last_update", null: false
    t.string "code", limit: 50, null: false
    t.string "matricula", limit: 50, null: false
    t.string "file_url", limit: 500
  end

  create_table "certificados", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "NroCertificado"
    t.datetime "VigenciaDesdeC"
    t.datetime "VigenciaHastaC"
    t.datetime "FechaEmisionOrig"
    t.datetime "FechaAnulacionC"
    t.integer "CodMotivoAnulacion"
    t.integer "CodCausaAnulacion"
    t.boolean "EsPrendaria"
    t.boolean "EsHipotecaria"
    t.bigint "prenda_id"
    t.string "datos_particulares_type"
    t.bigint "forma_pago_id"
    t.bigint "poliza_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["forma_pago_id"], name: "index_certificados_on_forma_pago_id"
    t.index ["poliza_id"], name: "index_certificados_on_poliza_id"
    t.index ["prenda_id"], name: "index_certificados_on_prenda_id"
  end

  create_table "clientes", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "nombre", limit: 50, null: false
    t.string "apellido", limit: 50, null: false
    t.date "fecha_nacimiento"
    t.integer "sexo", limit: 2, comment: "1-Masculino | 2-Femenino"
    t.integer "estado_civil", limit: 2
    t.string "mail", limit: 200
    t.string "password", limit: 60
    t.string "celular", limit: 50
    t.string "cuit", limit: 13
    t.string "tipo_dni", limit: 3, default: "1", null: false
    t.string "dni", limit: 15, null: false
    t.integer "tarjeta_credito", limit: 2
    t.string "numero_tarjeta", limit: 250
    t.string "cbu", limit: 30
    t.string "direccion", limit: 100, null: false
    t.string "dir_numero", limit: 25
    t.string "dir_piso", limit: 10
    t.string "dir_departamento", limit: 10
    t.string "codigo_postal", limit: 8, null: false
    t.integer "provincia_id", limit: 2
    t.string "dir_telefono", limit: 50
    t.integer "seller_id"
    t.integer "producer_id", null: false
    t.datetime "last_access"
    t.datetime "creation_date"
    t.integer "prospecto", limit: 2, default: 1, null: false
    t.integer "in_icommkt", limit: 2, default: 0, null: false
    t.date "convertion_date"
  end

  create_table "clients", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "codRol"
    t.string "DescRol"
    t.string "TipoPersona"
    t.string "ApellidoNombre"
    t.string "Direccion"
    t.string "codCliente"
    t.bigint "NroDocumento"
    t.string "codTipoDocumento"
    t.string "DescTipoDocumento"
    t.string "NroCUIT"
    t.string "codCondicionIva"
    t.string "DescCondicionIva"
    t.string "CategMonoributo"
    t.string "DescCategMonotributo"
    t.integer "CodPostal"
    t.integer "codLocalidad"
    t.string "Localidad"
    t.integer "codProvincia"
    t.string "Provincia"
    t.string "email"
    t.integer "codSexo"
    t.string "DescSexo"
    t.datetime "FechaNacimiento"
    t.integer "codEstadoCivil"
    t.string "DescEstadoCivil"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_clients_on_certificado_id"
  end

  create_table "coberturas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_cotizacion", null: false
    t.string "id_cobertura_comp", limit: 50, null: false
    t.string "titulo", limit: 150, default: "", null: false
    t.text "descripcion", null: false
    t.decimal "premio", precision: 10, scale: 2
    t.decimal "prima", precision: 10, scale: 2
    t.integer "franquicia"
    t.string "valor_franquicia", limit: 50
    t.integer "cuotas", limit: 2, null: false
    t.decimal "valor_cuota", precision: 10, scale: 2
    t.decimal "comision_productor", precision: 10, scale: 2
    t.index ["id"], name: "id"
  end

  create_table "coberturas_ap", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_ap", null: false
    t.integer "id_cobertura", limit: 2, default: 1, null: false
    t.string "suma_asegurada", limit: 15, null: false
    t.string "tasa", limit: 5, default: "0", null: false
    t.string "prima", limit: 15
    t.string "deducible", limit: 150
  end

  create_table "coberturas_integrales", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_cobertura", limit: 2
    t.integer "id_ubicacion"
    t.decimal "suma", precision: 10, scale: 2
    t.decimal "tasa", precision: 5, scale: 2
    t.decimal "prima", precision: 10, scale: 2
    t.string "deducible", limit: 250
    t.integer "medida_prestacion", limit: 2
    t.integer "ambito", limit: 2
  end

  create_table "coberturas_presupuestos", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_presupuesto", null: false
    t.string "id_cobertura_comp", limit: 10, null: false
  end

  create_table "codigos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "producer_id"
    t.integer "company_id", null: false
    t.string "name", limit: 40, null: false
    t.integer "matricula"
    t.string "codigo", limit: 20, null: false
    t.string "casa", limit: 25
    t.string "id_organizador", limit: 10
    t.integer "comision", limit: 2
    t.string "mail_code", limit: 75
    t.string "code_user", limit: 150
    t.string "code_password", limit: 150
    t.integer "check_rnw", limit: 2, default: 1, null: false
    t.integer "entorno", limit: 2, default: 1, null: false
    t.datetime "last_update", default: "2019-01-01 00:00:00", null: false
    t.integer "download", limit: 2, default: 0, null: false
  end

  create_table "comision_generadas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "Intermed"
    t.float "Monto"
    t.float "Porcentaje"
    t.bigint "recibo_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recibo_id"], name: "index_comision_generadas_on_recibo_id"
  end

  create_table "companies", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 75, null: false
    t.string "short_name", limit: 25
    t.string "telefono_grua", limit: 100
    t.string "url_logo", limit: 100
    t.string "slug", limit: 50
    t.string "ssn_id", limit: 10
    t.integer "ws", limit: 2, default: 0, null: false
    t.integer "active", limit: 2, default: 1, null: false
    t.index ["id"], name: "id", unique: true
  end

  create_table "consultas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "infoauto_id"
    t.integer "modo_de_pago"
    t.integer "cantidad_cuotas", limit: 2
    t.string "marca", limit: 50
    t.integer "marca_id"
    t.string "modelo", limit: 50
    t.integer "modelo_id"
    t.string "version", limit: 75
    t.integer "version_id"
    t.integer "anio"
    t.integer "sumaasegurada", default: 0, null: false
    t.string "origen", limit: 5, default: "AR", null: false
    t.integer "categoria"
    t.integer "capacidad", limit: 2
    t.integer "tipo_auto"
    t.string "tipo_persona", limit: 5, default: "F"
    t.string "condicion_iva", limit: 5, default: "CF"
    t.string "alternativa_comercial_san_cristobal", limit: 100
    t.string "descuento_especial_el_norte", limit: 100
    t.string "plan_orbis", limit: 100
    t.string "vigencia_orbis", limit: 100
    t.integer "ajuste_automatico"
    t.integer "productor_id", null: false
    t.integer "seller_id", default: 0, null: false
    t.integer "multicotizador"
    t.integer "campaign"
    t.integer "cliente_id"
    t.string "client_type", limit: 1, default: "F", null: false
    t.integer "codigo_postal"
    t.string "localidad_nombre", limit: 100
    t.string "provincia_id", limit: 3
    t.decimal "sumaaccesorios", precision: 10, scale: 2
    t.string "accesorios", limit: 250
    t.integer "sumagnc"
    t.date "vigencia"
    t.integer "combustibleadicional"
    t.integer "tipo_uso"
    t.integer "cero_km", limit: 2, default: 0, null: false
    t.datetime "creation_date"
  end

  create_table "cotizaciones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "consulta", null: false
    t.integer "company", limit: 2, null: false
    t.integer "company_id", null: false
    t.integer "sumaasegurada", null: false
    t.integer "valor_0km", null: false
    t.integer "producer_id", null: false
    t.integer "seller_id", null: false
    t.integer "multicotizador"
    t.integer "campaign"
    t.datetime "creation_date"
  end

  create_table "coverages", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "FechaInicioVigenciaModulo"
    t.integer "CodModulo"
    t.string "DescModulo"
    t.string "DescModulo2"
    t.datetime "FechaInicioVigenciaCobertura"
    t.integer "CodCobertura"
    t.string "DescCobertura"
    t.float "SumaAsegurada"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_coverages_on_certificado_id"
  end

  create_table "cuotas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id", null: false
    t.integer "nro_cuota", limit: 2, null: false
    t.decimal "monto", precision: 10, null: false
    t.date "vto_company"
    t.date "vto_convenio"
    t.integer "status", limit: 2, default: 1, null: false
    t.decimal "saldo_pagado", precision: 10, default: "0", null: false
    t.decimal "resto", precision: 10
    t.datetime "payment_date"
  end

  create_table "datos_particulares_accidentes_personales", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "NroActividad"
    t.string "ActividadAsegurado"
    t.integer "CodClasificacion"
    t.string "Clasificacion"
    t.integer "Grupo"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_accidentes_personales_on_certificado_id"
  end

  create_table "datos_particulares_caucions", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "TipoCaucion"
    t.string "AfectaCapital"
    t.string "NotaCobertura"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_caucions_on_certificado_id"
  end

  create_table "datos_particulares_combinado_familiars", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "TipoSeguro"
    t.string "Actividad"
    t.string "ZonaRiesgo"
    t.string "ObjDiversosBienesAsegurados"
    t.float "PackVerano"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_combinado_familiars_on_certificado_id"
  end

  create_table "datos_particulares_incendios", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "Actividad"
    t.string "Detalle"
    t.string "TipoConstruccion"
    t.integer "CantidadPisos"
    t.string "TipoCerramiento"
    t.string "ImprimirArticulo"
    t.float "PrimaDeposito"
    t.float "LucroCesante"
    t.string "TipoTecho"
    t.float "PrimaAnticipo"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_incendios_on_certificado_id"
  end

  create_table "datos_particulares_resp_civils", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "Actividad"
    t.float "Capital"
    t.string "ZonaRiesgo"
    t.string "Inspeccion"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_resp_civils_on_certificado_id"
  end

  create_table "datos_particulares_seguro_integrals", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "TipoSeguro"
    t.string "Actividad"
    t.string "ZonaRiesgo"
    t.string "ObjDiversosBienesAsegurados"
    t.bigint "robo_id"
    t.bigint "incendio_id"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_seguro_integrals_on_certificado_id"
    t.index ["incendio_id"], name: "index_datos_particulares_seguro_integrals_on_incendio_id"
    t.index ["robo_id"], name: "index_datos_particulares_seguro_integrals_on_robo_id"
  end

  create_table "datos_particulares_seguro_tecnicos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "TipoSeguro"
    t.string "Actividad"
    t.string "ZonaRiesgo"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_seguro_tecnicos_on_certificado_id"
  end

  create_table "datos_particulares_transportes", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.float "LimMaxViaje"
    t.string "Moneda"
    t.string "FrecDeclaracion"
    t.string "TipoMercaderia"
    t.string "MercaderiaInternacional"
    t.string "Origen"
    t.string "Destino"
    t.float "KmRecorridos"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_transportes_on_certificado_id"
  end

  create_table "datos_particulares_vida_colectivos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "EdadReal"
    t.integer "EdadActuarial"
    t.float "CapitalAsegurado"
    t.float "Prima"
    t.string "DuracionSeguro"
    t.string "DuracionPagos"
    t.integer "EdadMax"
    t.datetime "FechaIngTrabajo"
    t.float "Sueldo"
    t.string "MediaJornada"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_datos_particulares_vida_colectivos_on_certificado_id"
  end

  create_table "desarrollo_premio", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id", null: false
    t.decimal "prima", precision: 10, scale: 2
    t.decimal "recargo_administrativo", precision: 10, scale: 2
    t.decimal "derecho_emision", precision: 10, scale: 2
    t.decimal "prima_ssn", precision: 10, scale: 2
    t.decimal "recargo_financiero", precision: 10, scale: 2
    t.decimal "iva", precision: 10, scale: 2
    t.decimal "iibb", precision: 10, scale: 2
    t.decimal "sellado", precision: 10, scale: 2
    t.decimal "comision", precision: 10, scale: 2
    t.decimal "premio", precision: 10, scale: 2
    t.integer "cant_cuotas", limit: 2
    t.decimal "valor_cuota", precision: 10, scale: 2
  end

  create_table "detalles_coberturas_integrales", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_cobertura", null: false
    t.string "detalle", limit: 250, null: false
    t.decimal "valor", precision: 10, scale: 2
    t.integer "cantidad", default: 1
  end

  create_table "dtos_cias_consultas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "consulta_id", null: false
    t.string "producer_code", limit: 75, null: false
    t.integer "plan", limit: 2
    t.integer "descuento", limit: 2
    t.integer "descuento_extra", limit: 2
    t.string "facturacion", limit: 1
    t.integer "comision", limit: 2
    t.integer "company_id", null: false
  end

  create_table "emisiones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id", null: false
    t.integer "estado", limit: 2, default: 1, null: false
    t.string "url_pdf", limit: 250
    t.string "nro_poliza", limit: 100
    t.string "nro_certificado", limit: 100
    t.integer "incidente_id"
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.datetime "creation_date"
    t.integer "is_manual", limit: 2, default: 0, null: false
  end

  create_table "emisiones_pendientes", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_propuesta", null: false
    t.integer "producer_id", null: false
    t.integer "seller_id", null: false
    t.datetime "last_try", null: false
  end

  create_table "empresas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "real_name", limit: 150, null: false
    t.string "fantasy_name", limit: 150
    t.integer "id_organizacion"
    t.string "cuit", limit: 15
    t.string "condicion_iva", limit: 2
    t.string "address", limit: 200
    t.string "address_number", limit: 25
    t.string "address_piso", limit: 10
    t.string "address_dpto", limit: 10
    t.string "codigo_postal", limit: 5
    t.integer "state_id", limit: 2
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.string "telephone", limit: 30
    t.string "mail", limit: 75
    t.string "rubro_id", limit: 10
    t.integer "prospecto", limit: 2, default: 1, null: false
    t.datetime "convertion_date"
    t.datetime "creation_date"
  end

  create_table "forma_pagos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "FechaInicioVigencia"
    t.integer "TipoPago"
    t.string "DescTipoPago"
    t.string "NroCBU"
    t.integer "CodBanco"
    t.integer "codTarjetaCredito"
    t.string "DescTarjetaCredito"
    t.string "NroTarjetaCredito"
    t.bigint "NroCuenta"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "fuel_type", id: { type: :integer, limit: 2, default: nil }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "fuel_type", limit: 50, null: false
  end

  create_table "incendios", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.float "PrimaDeposito"
    t.string "Actividad"
    t.string "TipoConstruccion"
    t.integer "CantidadPisos"
    t.string "TipoRiesgo"
    t.string "TipoCerramiento"
    t.integer "PeriodoIndemnizacion"
    t.string "TipoTecho"
    t.float "PrimaAnticipo"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "inspecciones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "emision_id", null: false
    t.integer "side", limit: 2, default: 1, null: false
    t.string "picture", limit: 250, null: false
    t.integer "status", limit: 2, null: false
    t.datetime "creation_date", null: false
  end

  create_table "inspecciones_img", id: :integer, charset: "latin1", force: :cascade do |t|
    t.string "img_url", limit: 250, null: false
    t.integer "propuesta_id", null: false
  end

  create_table "integrales_comercio_consorcio", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_riesgo_integral", null: false
    t.integer "tipo_integral", limit: 2, default: 1, null: false
    t.integer "id_empresa", null: false
    t.integer "id_company", null: false
    t.string "cobertura", limit: 150, null: false
    t.decimal "suma_asegurada", precision: 10, scale: 2, null: false
    t.string "prima", limit: 20
    t.string "premio", limit: 20
    t.string "comision", limit: 20
    t.string "deducible", limit: 20
    t.integer "tipo_moneda", limit: 2, default: 1, null: false
    t.string "tipo_riesgo", limit: 250
    t.text "observaciones"
    t.string "file"
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.datetime "creation_date"
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "items", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "CodigoGrupo"
    t.string "TipoBien"
    t.string "DescripcionBien"
    t.float "Capital"
    t.float "Franquicia"
    t.string "Notas"
    t.integer "CódigoGrupo"
    t.string "DescBienOrig"
    t.string "Descripcion"
    t.string "datos_particulares_type"
    t.bigint "datos_particulares_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["datos_particulares_type", "datos_particulares_id"], name: "index_items_on_datos_particulares"
  end

  create_table "iva_condition", id: { type: :integer, limit: 2, default: nil }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "iva_condition", limit: 25, null: false
  end

  create_table "matriculas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 40, null: false
    t.string "matricula", limit: 50, null: false
    t.string "cuit", limit: 13
    t.integer "producer_id", null: false
  end

  create_table "mov_recibos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "idMovimiento"
    t.integer "codTipoMovimiento"
    t.string "DescTipoMovimiento"
    t.datetime "FechaContabilizacion"
    t.datetime "FechaEfecto"
    t.datetime "FechaCobro"
    t.float "Importe"
    t.integer "NroTerminal"
    t.integer "NroLote"
    t.bigint "recibo_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recibo_id"], name: "index_mov_recibos_on_recibo_id"
  end

  create_table "movimientos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "FechaEfecto"
    t.datetime "FechaContable"
    t.datetime "FechaImpresion"
    t.integer "NroEndoso"
    t.integer "CodMovimientoGral"
    t.integer "NroRecibo"
    t.float "Monto"
    t.integer "CodTipo"
    t.string "DescTipoMovimiento"
    t.integer "NroOficialMov"
    t.bigint "PolizaAnterior"
    t.string "NroTramite"
    t.integer "NroMotivoModificacion"
    t.string "DescMotivoModificacion"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_movimientos_on_certificado_id"
  end

  create_table "nominas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 75
    t.string "dni", limit: 10
    t.date "birthday"
    t.string "mail", limit: 150
    t.string "telephone", limit: 20
    t.date "fecha_alta_empresa"
    t.string "cuit", limit: 14
    t.integer "tipo_beneficiario", limit: 2
    t.integer "tipo", limit: 2
    t.integer "id_propuesta", null: false
  end

  create_table "normalizer_accessories", id: { type: :integer, limit: 2 }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 25, null: false
    t.integer "type", limit: 2, default: 1, null: false, comment: "1-Acceosrio | 2-GNC"
  end

  create_table "normalizer_automatic_adjustment", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "adjustment_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "adjustment", limit: 3, null: false
  end

  create_table "normalizer_fuel_type", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "fuel_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "fuel_type", limit: 5, null: false
  end

  create_table "normalizer_iva_condition", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "iva_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "condition_company", limit: 3, null: false
  end

  create_table "normalizer_pay_way", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "pay_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "pay", limit: 25, null: false
  end

  create_table "normalizer_person_type", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "person_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "type_company", limit: 15, null: false
  end

  create_table "normalizer_states", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "state_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "state_code", limit: 5, null: false
  end

  create_table "normalizer_use_type", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "use_id", limit: 2, null: false
    t.integer "company_id", null: false
    t.string "use_type", limit: 25, null: false
  end

  create_table "objetivos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.integer "creator_id", null: false
    t.date "fecha", null: false
    t.decimal "objetivo_prima", precision: 10, scale: 2, null: false
    t.integer "objetivo_operaciones", null: false
    t.decimal "total_prima", precision: 10, scale: 2, default: "0.0", null: false
    t.integer "total_operaciones", default: 0, null: false
  end

  create_table "organizaciones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 150, null: false
    t.integer "id_categoria", limit: 2, null: false
    t.integer "id_empresa"
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.datetime "creation_date"
  end

  create_table "paseguros_active_codes", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "code_id", null: false
    t.integer "producer_id", null: false
    t.integer "company_id", null: false
    t.string "plan", limit: 20
    t.string "descuento", limit: 20
    t.string "descuento_extra", limit: 20
    t.string "facturacion", limit: 20
    t.string "comision", limit: 20
    t.integer "use_multi", limit: 2, default: 0, null: false
    t.integer "use_paseguros", limit: 2, default: 0, null: false
  end

  create_table "pay_way", id: { type: :integer, limit: 2, default: nil }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "pay_way", limit: 25, null: false
  end

  create_table "payments", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "producer_id", null: false
    t.integer "plan_id", limit: 2, null: false
    t.integer "amount", null: false
    t.date "expiration", null: false
    t.integer "status", limit: 2, null: false, comment: "1-Pendiente | 2-Próximo a vencer | 3-Pagado | 4-Vencido"
  end

  create_table "person_type", id: { type: :integer, limit: 2, default: nil }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "person_type", limit: 15, null: false
  end

  create_table "plans", id: { type: :integer, limit: 2 }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "amount", limit: 2, null: false
    t.integer "sellers", limit: 2, default: 1, null: false
  end

  create_table "polizas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "TipoPoliza"
    t.string "DescTipoPoliza"
    t.bigint "NroOficialGral"
    t.datetime "VigenciaDesde"
    t.datetime "VigenciaHasta"
    t.datetime "FechaVigenciaInicial"
    t.datetime "FechaAnulacionPol"
    t.bigint "NroReferencia"
    t.string "NroPropuesta"
    t.integer "codRamo"
    t.string "DescRamo"
    t.integer "codFrecuenciaPago"
    t.string "DescFrecuenciaPago"
    t.integer "codGrupoEstadistico"
    t.integer "CantCuotas"
    t.integer "codTipoFacturacion"
    t.string "DescTipoFacturacion"
    t.integer "codProducto"
    t.string "DescProducto"
    t.integer "codMoneda"
    t.string "DescMoneda"
    t.bigint "codOrganizador"
    t.bigint "codProductor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "prendas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.datetime "FechaContrato"
    t.datetime "FechaEfecto"
    t.integer "CantCuotas"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "presupuestos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_cotizacion", null: false
    t.integer "id_consulta"
    t.integer "producer_id", null: false
    t.integer "seller_id"
    t.integer "cliente_id", null: false
    t.integer "multicotizador"
    t.integer "campaign"
    t.datetime "creation_date", null: false
  end

  create_table "producer_permission", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_producer", null: false
    t.integer "id_company", null: false
  end

  create_table "producers", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "razon_social", limit: 150
    t.string "name", limit: 100, null: false
    t.string "person_type", limit: 1, default: "F", null: false
    t.string "condicion_iva", limit: 2
    t.integer "recibe_factura", limit: 2, default: 0, null: false
    t.integer "paga", limit: 2, default: 1, null: false
    t.string "mail", limit: 100, null: false
    t.string "phone", limit: 25
    t.string "location", limit: 75
    t.string "location_dpto", limit: 50
    t.string "location_piso", limit: 50
    t.string "location_city", limit: 30
    t.integer "location_state", limit: 2
    t.string "location_lat", limit: 100
    t.string "location_long", limit: 100
    t.integer "rightnow_id"
    t.integer "rightnow_organizacion_id"
    t.string "cuit", limit: 13
    t.integer "san_cristobal_casa", limit: 2
    t.integer "san_cristobal_id"
    t.integer "san_cristobal_id_organizador"
    t.integer "san_cristobal_entorno", limit: 2, default: 1, null: false
    t.integer "el_norte_casa", limit: 2
    t.integer "el_norte_id"
    t.integer "el_norte_id_organizador"
    t.integer "el_norte_entorno", limit: 2, default: 1, null: false
    t.bigint "orbis_id"
    t.integer "orbis_id_organizador"
    t.integer "orbis_entorno", limit: 2, default: 1, null: false
    t.integer "hdi_id"
    t.integer "hdi_entorno", limit: 2, default: 1, null: false
    t.string "description", limit: 250
    t.string "vinculante1_name", limit: 100
    t.integer "vinculante1_comision", limit: 2
    t.string "vinculante2_name", limit: 100
    t.integer "vinculante2_comision", limit: 2
    t.string "vinculante3_name", limit: 100
    t.integer "vinculante3_comision", limit: 2
    t.datetime "creation_date", null: false
    t.integer "cant_sellers", limit: 2, default: 1, null: false
    t.integer "plan_id", limit: 2, default: 1
    t.integer "payment_status", limit: 2, default: 1, null: false
    t.string "logo_url", limit: 250
    t.integer "logo_size", limit: 2
    t.integer "use_multi", limit: 2, default: 0, null: false
    t.datetime "accept_terms"
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "propuestas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "status", limit: 2, default: 7, null: false
    t.integer "cotizacion_id", null: false
    t.integer "cobertura_id"
    t.string "cobertura_id_comp", limit: 25, null: false
    t.integer "franquicia_id", default: 0, null: false
    t.integer "cliente_id", null: false
    t.string "client_type", limit: 1, default: "F", null: false
    t.integer "pw_type", limit: 2
    t.integer "pw_id"
    t.decimal "suma_asegurada", precision: 10, scale: 2
    t.string "patente", limit: 25, null: false
    t.string "motor", limit: 100, null: false
    t.string "chasis", limit: 100, null: false
    t.string "acreedor_prendario", limit: 100
    t.string "inspeccion_previa", limit: 50
    t.date "inspeccion_fecha"
    t.string "inspeccion_kilometros", limit: 15
    t.string "inspeccion_tipo_combustible", limit: 50
    t.string "inspeccion_color", limit: 25
    t.string "direcciones_de_inspeccion", limit: 50
    t.text "comments"
    t.integer "company_id", null: false
    t.integer "estado", limit: 2, default: 1, null: false
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.integer "manual", limit: 2, default: 0, null: false
    t.datetime "propuesta_date"
    t.datetime "creation_date", null: false
  end

  create_table "propuestas_ramos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "type", limit: 2, null: false
    t.integer "id_ramo"
    t.string "person_type", limit: 1, default: "F", null: false
    t.integer "client_id", null: false
    t.integer "pw_type", limit: 2, default: 1
    t.integer "pw_id"
    t.string "vigencia", limit: 1, default: "A", null: false
    t.date "inicio_vigencia", null: false
    t.date "fin_vigencia", null: false
    t.integer "moneda", limit: 2, default: 1, null: false
    t.string "nro_poliza", limit: 25
    t.string "file_url", limit: 250
    t.date "emision_date"
    t.integer "relacion_padre"
    t.integer "company_id", null: false
    t.integer "producer_id", null: false
    t.integer "seller_id", null: false
    t.string "producer_code", limit: 25
    t.datetime "creation_date", null: false
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "pw", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "client_id", null: false
    t.integer "type_client", limit: 2, null: false
    t.string "numero", limit: 25
    t.integer "type", limit: 2
  end

  create_table "pw_cc", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "client_id", null: false
    t.string "client_type", limit: 1, default: "F", null: false
    t.integer "type", limit: 2
    t.string "cnumber", limit: 250, null: false
    t.string "name", limit: 25, null: false
    t.string "lastname", limit: 25, null: false
    t.string "vto_mes", limit: 2, null: false
    t.integer "vto_ano", null: false
    t.string "bank_id", limit: 5, default: "00001", null: false
  end

  create_table "pw_da", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "client_id", null: false
    t.string "client_type", limit: 1, default: "F", null: false
    t.string "cbu_number", limit: 250, null: false
    t.string "bank_id", limit: 5, default: "00001"
  end

  create_table "ramo_ap", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "plan_id", limit: 2
    t.integer "ambito_id", limit: 2
  end

  create_table "ramo_automoviles", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "vehicle_id", null: false
    t.integer "propuesta_id", null: false
  end

  create_table "ramo_comercio", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "actividad_id", limit: 10, null: false
    t.text "description"
    t.integer "cant_capita", limit: 2
    t.integer "masa_salarial"
  end

  create_table "ramo_consorcio", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.text "description"
    t.integer "admin_id"
    t.integer "cant_capita", limit: 2
    t.integer "masa_salarial"
    t.integer "propuesta_id", null: false
  end

  create_table "ramo_familiar", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "tipo_vivienda", limit: 2, null: false
  end

  create_table "ramo_otros", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id", null: false
    t.decimal "suma_asegurada", precision: 10, null: false
    t.decimal "tasa", precision: 2, null: false
    t.decimal "prima", precision: 10, null: false
    t.text "description", null: false
  end

  create_table "ramo_vc", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "convenio_id", limit: 2, default: 1, null: false
    t.string "suma_asegurada", limit: 10
  end

  create_table "ramo_vo", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "convenio_id", limit: 2, default: 1, null: false
    t.string "suma_asegurada", limit: 10
  end

  create_table "recibos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "NroRecibo"
    t.bigint "NroReciboBase"
    t.datetime "VigenciaDesde"
    t.datetime "VigenciaHasta"
    t.datetime "FechaVencimiento"
    t.integer "NroTransaccion"
    t.float "FactorCambio"
    t.float "ImportePremio"
    t.float "ImporteSaldo"
    t.float "ImportePrima"
    t.float "PrimaAnual"
    t.integer "Periodo"
    t.float "PremioAnual"
    t.float "ImporteIVAGeneral"
    t.float "ImporteDescuento"
    t.float "ImporteTasasImpuestos"
    t.float "ImporteRedondeo"
    t.float "ImporteIVAPercepcion"
    t.float "ImporteIngBrutosPercep"
    t.float "ImporteCargoFinanciero"
    t.float "ImpuestoSellos"
    t.float "CapitalSocial"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_recibos_on_certificado_id"
  end

  create_table "referidos", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_referido", null: false
    t.integer "id_referente"
    t.integer "id_categoria", limit: 2, null: false
    t.integer "id_subcategoria", limit: 2, null: false
    t.integer "id_organizacion"
  end

  create_table "riesgo_integral", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "provincia_id", limit: 2, null: false
    t.string "codigo_postal", limit: 5, null: false
    t.string "ciudad", limit: 100
    t.string "direccion", limit: 100, null: false
    t.string "numero", limit: 10
    t.string "piso", limit: 10
    t.string "departamento", limit: 10
    t.integer "seller_id", null: false
    t.integer "producer_id", null: false
    t.datetime "creation_date"
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "robos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "Zona"
    t.string "Actividad"
    t.float "ValorRiesgo"
    t.string "ModalidadSeguro"
    t.string "TipoRiesgo"
    t.string "Moneda"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rubricas", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "producer_id", null: false
    t.string "producer_mat", limit: 25
    t.string "producer_person_type", limit: 1
    t.datetime "creation_date"
    t.string "client_type", limit: 1
    t.string "client_doc_type", limit: 2
    t.string "client_doc_number", limit: 20
    t.string "client_name", limit: 150
    t.string "client_cpa", limit: 8
    t.string "client_address", limit: 250
    t.string "cia_id", limit: 5
    t.string "bien_asegurado", limit: 250
    t.integer "ramo_id", limit: 2
    t.decimal "suma_asegurada", precision: 10, scale: 2, default: "0.0"
    t.integer "suma_asegurada_tipo", limit: 2
    t.date "desde"
    t.date "hasta"
    t.integer "operation_type", limit: 2
    t.integer "flota", limit: 2, default: 0, null: false
    t.integer "contact_type", limit: 2, default: 4, null: false
    t.integer "origin"
    t.integer "is_manual", limit: 2, default: 0, null: false
  end

  create_table "sections", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "section", limit: 50, null: false, collation: "latin1_swedish_ci"
    t.string "url_link", limit: 50, null: false, collation: "latin1_swedish_ci"
    t.string "icon", limit: 50, default: "fa fa-folder-o", null: false
    t.integer "is_button", default: 0, null: false
    t.integer "sub_de", default: 0, null: false
    t.integer "activo", default: 1, null: false
    t.integer "orden", default: 0, null: false
  end

  create_table "sellers", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_producer", null: false
    t.integer "is_admin", limit: 2, default: 0, null: false
    t.integer "see_all", limit: 2, default: 0, null: false
    t.string "name", limit: 50, null: false
    t.string "lastname", limit: 50, null: false
    t.string "user", limit: 150, null: false
    t.string "mail", limit: 150, null: false
    t.string "password", limit: 60, null: false
    t.integer "rightnow_id"
    t.datetime "creation_date", null: false
    t.datetime "last_access"
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "sellers_codes", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "seller_id", null: false
    t.integer "code_id", null: false
  end

  create_table "sellers_login_history", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_seller", null: false
    t.datetime "login_date", null: false
  end

  create_table "states", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "state", limit: 150, null: false
    t.string "map_code", limit: 5, default: "S/D", null: false
  end

  create_table "sublimites_cobertura_integrales", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "id_cobertura", null: false
    t.integer "id_sublimite", limit: 2, null: false
    t.decimal "suma", precision: 10, scale: 2
    t.decimal "deducible", precision: 10, scale: 2
  end

  create_table "telefonos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "numero"
    t.bigint "client_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_id"], name: "index_telefonos_on_client_id"
  end

  create_table "tests", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "numero"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ubicacion_riesgos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "Domicilio"
    t.integer "codLocalidad"
    t.string "Localidad"
    t.integer "CodPostal"
    t.integer "codProvincia"
    t.string "Provincia"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_ubicacion_riesgos_on_certificado_id"
  end

  create_table "ubicaciones", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "client_id", null: false
    t.integer "type_client", limit: 2, null: false
    t.integer "zip_code", limit: 2
    t.string "city", limit: 75
    t.integer "state", limit: 2
    t.string "street", limit: 75
    t.string "number", limit: 20
    t.string "piso", limit: 2
    t.string "dpto", limit: 5
    t.string "telephone", limit: 20
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "ubicaciones_riesgos", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "propuesta_id", null: false
    t.integer "zip_code", limit: 2
    t.string "city", limit: 75
    t.integer "state_id", limit: 2
    t.string "street", limit: 75
    t.string "number", limit: 20
    t.string "piso", limit: 2
    t.string "dpto", limit: 5
  end

  create_table "use_type", id: { type: :integer, limit: 2 }, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.string "use_type", limit: 25, null: false
  end

  create_table "user_permissions", id: false, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "modulo_id", null: false
  end

  create_table "users", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "admin", default: 0, null: false
    t.string "name", limit: 50, null: false
    t.string "lastname", limit: 50, null: false
    t.string "mail", limit: 150, null: false
    t.string "user", limit: 20, null: false
    t.string "password", limit: 60, null: false
    t.datetime "last_access"
    t.integer "active", default: 1, null: false
  end

  create_table "vehicles", id: :integer, charset: "utf8mb3", collation: "utf8_spanish_ci", force: :cascade do |t|
    t.integer "infoauto_id"
    t.string "marca", limit: 50
    t.integer "marca_id", null: false
    t.string "modelo", limit: 50
    t.integer "modelo_id", null: false
    t.string "version", limit: 75
    t.integer "version_id", null: false
    t.integer "anio"
    t.string "color", limit: 30
    t.string "patente", limit: 10
    t.string "motor", limit: 20, null: false
    t.string "chasis", limit: 20, null: false
    t.decimal "suma_asegurada", precision: 10, scale: 2
    t.decimal "gnc", precision: 10, scale: 2
    t.text "gnc_data"
    t.decimal "accesories", precision: 10, scale: 2
    t.text "accesories_data"
    t.integer "producer_id", null: false
    t.datetime "creation_date", null: false
    t.integer "active", limit: 2, default: 1, null: false
  end

  create_table "vehiculos", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "CodVehiculo"
    t.string "DescVehiculo"
    t.string "NroMotor"
    t.string "NroChasis"
    t.string "CodRegistro"
    t.integer "ModeloAño"
    t.boolean "GuardaEnGarage"
    t.boolean "ConductoresMenoresDe25"
    t.boolean "Hasta25MilKmAnuales"
    t.integer "codTipoAsistencia"
    t.string "DescTipoAsistencia"
    t.integer "codTipoCombustible"
    t.string "DescTipoCombustible"
    t.float "SumaAsegurada"
    t.integer "codUsoVehiculo"
    t.string "DescUsoVehiculo"
    t.integer "codClaseVehiculo"
    t.string "DescClaseVehiculo"
    t.integer "codMarcaEquipoRastreo"
    t.string "DescMarcaEquipoRastreo"
    t.float "ValorGNC"
    t.float "DescuentoSiniestralidad"
    t.float "AjusteAutomatico"
    t.string "MarcaRegulador"
    t.string "NroRegulador"
    t.string "MarcaCilindro1"
    t.string "NroCilindro1"
    t.string "MarcaCilindro2"
    t.string "NroCilindro2"
    t.bigint "certificado_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["certificado_id"], name: "index_vehiculos_on_certificado_id"
  end

  create_table "vto_cuotas", id: :integer, charset: "latin1", force: :cascade do |t|
    t.integer "emision_id", null: false
    t.decimal "monto", precision: 10, scale: 2, null: false
    t.date "vto", null: false
    t.integer "estado", limit: 2, default: 1, null: false
  end

end
